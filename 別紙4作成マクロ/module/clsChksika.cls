VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsChk"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


Public Function ChkMain(ByRef ws As Worksheet) As Boolean
    'エラーカウンタ
    Dim cnterr As Long


    '必須項目チェック
    cnterr = cnterr + ChkNecessary(ws)


    '日付関連チェック
    cnterr = cnterr + ChkDate(ws)
    

    'ほみそ入力チェック
    Dim cntHomiso As Integer
    cntHomiso = ChkHOMISO(ws)
    If cntHomiso > 0 Then
        If MsgBox("[保み想]が点数のないの箇所にありますが宜しいですか？", _
                vbQuestion + vbYesNo + vbDefaultButton2) = vbYes Then
            MsgBox "[保み想]エラーをすべて解除します", vbInformation
            
            Call RemoveHomisoError(ws)
            
        Else
            cnterr = cnterr + cntHomiso
        End If
    End If
    
    
    '診療識別
    If Not ChkSinryoSikibetu(ws) Then
        MsgBox "入力間違いです", vbExclamation
        Exit Function
    End If
    
    
    'エラー数が０超の場合エラー
    If cnterr > 0 Then
        MsgBox "入力間違い " & cnterr & " 箇所です", vbExclamation
        Exit Function
    Else
        ChkMain = True
    End If
        
End Function

'チェックで入力ミスがあるセルは背景赤
Private Sub ErrCell(ByRef cnterr As Long, ByRef rng As Range)
    rng.Interior.Color = RGB(250, 0, 0)
    rng.Font.Color = RGB(255, 255, 255)
    cnterr = cnterr + 1
End Sub

'チェックで入力ミスがないセルは背景透明
Private Sub NoneErrCell(ByRef rng As Range)
    rng.Interior.ColorIndex = xlNone
    rng.Font.Color = RGB(0, 0, 0)
End Sub

'診療識別入力チェック
Public Function ChkSinryoSikibetu(ws As Worksheet) As Boolean
    
    Dim rngChk As Range
    Set rngChk = rngSinryoSikibetuCol
    Dim c As Range
    
    Dim cntInput As Long
    cntInput = 0
    
    '範囲内に一つでも何か入っていたらOK
    For Each c In rngChk.Cells
        If c.Value <> "" Then
            cntInput = cntInput + 1
            Exit For
        End If
    Next
    
    If cntInput = 0 Then
        '何もない場合赤にする
        rngChk.Interior.Color = RGB(255, 0, 0)
        ChkSinryoSikibetu = False
    Else
        '一つでもあればOKにする
        rngChk.Interior.ColorIndex = xlNone
        ChkSinryoSikibetu = True
    End If


End Function

'診療識別列エラー解除
Public Sub RemoveSinryoSikibetuErr(ws As Worksheet)
    Dim rngChk As Range
    Set rngChk = rngSinryoSikibetuCol


    rngChk.Interior.ColorIndex = xlNone
    

End Sub


'ほみそチェック
Public Function ChkHOMISO(ws As Worksheet) As Integer
    
    Dim rngChk As Range
    Set rngChk = rngKomoku
    Dim c As Range
    Dim cnterr As Long
    cnterr = 0
    
    For Each c In rngChk.Cells
    
        '点数、数量とも入っている
        If c.Offset(0, 1).Value <> "" And c.Offset(0, 2).Value <> "" Then
            
            '入っていないと背景を赤くする
            If c.Offset(0, 4) = "" Then
                Call ErrCell(cnterr, c.Offset(0, 4))
            Else
                Call NoneErrCell(c.Offset(0, 4))
            End If
            
        '点数、数量とも入ってない
        Else
            '入ってると背景を赤くする
            If c.Offset(0, 4) <> "" Then
                Call ErrCell(cnterr, c.Offset(0, 4))
            Else
                Call NoneErrCell(c.Offset(0, 4))
            End If
                        
        End If
    Next
    
    'エラー数を取得
    ChkHOMISO = cnterr
    
End Function
'ほみそエラーの解除
Public Sub RemoveHomisoError(ws As Worksheet)
    Dim rngChk As Range
    Set rngChk = rngKomoku
    Dim c As Range
  
    For Each c In rngChk.Cells
        Call NoneErrCell(c.Offset(0, 4))
    Next
    
End Sub

'必須項目入力チェック
Public Function ChkNecessary(ByRef ws As Worksheet) As Integer

    '個人情報＋管理情報、傷病名、実日数
    Dim rngNecessary As Range
    Dim cellsNecessary As String
    cellsNecessary = cellsKojin & "," & cellsManage
    Set rngNecessary = ws.Range(cellsNecessary)
    
    
    Dim c As Range
    Dim cnterr As Long
    
    For Each c In rngNecessary.Cells
        '空の場合エラーとする
        If c.Value = "" Then
            c.Interior.Color = RGB(255, 0, 0)
            cnterr = cnterr + 1
        Else
            c.Interior.Color = xlNone
        End If
    Next
    
    'エラー数を取得
    ChkNecessary = cnterr
       

End Function


'必須入力書式チェック
Private Function ChkFormatNecessary() As Boolean

    Set rngKoban = Range(cellsEdaban): rngKoban.Interior.ColorIndex = 0                           '項番
    Set rngSinryoKikanST = Range(cellsIryokikan): rngSinryoKikanST.Interior.ColorIndex = 0        '診療機関名
    Set rngSakuseisya = Range(cellsSakuseisya): rngSakuseisya.Interior.ColorIndex = 0             '作成者名
    Set rngSinryoYM = Range(cellsSinryoKikanST): rngSinryoYM.Interior.ColorIndex = 0              '診療年月開始
    Set rngSinryoKikanED = Range(cellsSinryoKikanED): rngSinryoKikanED.Interior.ColorIndex = 0    '診療年月終了
    Set rngSyobyo = Range(cellsSyobyo): rngSyobyo.Interior.ColorIndex = 0                         '傷病名
    Set rngNissu = Range(cellsNissu): rngNissu.Interior.ColorIndex = 0                            '実日数
    Set rngEdaban = Range(cellsEdaban): rngEdaban.Interior.ColorIndex = 0                         '枝番
    Set rngBirthDay = Range(cellsBirthday): rngBirthDay.Interior.ColorIndex = 0                   '生年月日
    
    
    '項番桁数
    If Len(rngKoban) < 11 Then
        rngKoban.Interior.Color = RGB(255, 0, 0)
        MsgBox "項番の桁数を確認してください", vbExclamation
        ChkFormatNecessary = False
        Exit Function
    End If
    
    '項番あたま2桁（神奈川支部）
    If Left(rngKoban, 2) <> "14" Then
        rngKoban.Interior.Color = RGB(0, 200, 200)
        If MsgBox("項番の左二桁が14ではありませんが宜しいですか？", _
            vbQuestion + vbYesNo + vbDefaultButton2) = vbNo Then
            ChkFormatNecessary = False
            Exit Function
        End If
    End If
    
    
    '半角英大数に
    rngKoban.Value = StrConv(rngKoban.Value, vbNarrow + vbUpperCase)


End Function

Public Function ChkDate(ByRef ws As Worksheet) As Integer
    Dim cnterr As Long
    
    If Not IsDate(rngSinryoKikanST) Then
        Call ErrCell(cnterr, rngSinryoKikanST)
    Else
        Call NoneErrCell(rngSinryoKikanST)
    End If
    
    If Not IsDate(rngSinryoKikanED) Then
        Call ErrCell(cnterr, rngSinryoKikanED)
    Else
        Call NoneErrCell(rngSinryoKikanED)
    End If
    
    
    If Not IsDate(rngSinryoYM) Then
        Call ErrCell(cnterr, rngSinryoYM)
    Else
        Call NoneErrCell(rngSinryoYM)
    End If
    
    If Not IsDate(rngBirthDay) Then
        Call ErrCell(cnterr, rngBirthDay)
    Else
        Call NoneErrCell(rngBirthDay)
    End If
        
    ChkDate = cnterr
    
End Function

Public Function ChangeDate() As String
    
    '日付書式
    rngSinryoKikanST.Value = ChangeDateFormat(rngSinryoKikanST.Value) '診療期間終了
    rngSinryoKikanED.Value = ChangeDateFormat(rngSinryoKikanED.Value) '診療期間開始
    
    rngSinryoYM.Value = ChangeDateFormat(rngSinryoYM.Value) '診療年月
    rngBirthDay.Value = ChangeDateFormat(rngBirthDay.Value) '生年月日
        
End Function


'日付書式変換
Public Function ChangeDateFormat(strVal As String) As String
    Dim res As String
    Const HEISEI As Integer = 1988
    Const SHOWA As Integer = 1925
    
    If strVal = "" Then Exit Function


    Dim strWareki As String
    Select Case Left(strVal, 1)
        Case "1": strWareki = "明治"
        Case "2": strWareki = "大正"
        Case "3": strWareki = "昭和"
        Case "4": strWareki = "平成"
        
    End Select

    Dim y As Integer
    y = 0

    Select Case Len(strVal)
        Case 4
            'eemm 2810
            '年月のみ
            Select Case CInt(Left(strVal, 2))
                 '昭和
                 Case Is > Format(Now, "ee") + 1
                     y = Left(strVal, 2) + SHOWA
                 
                 '平成
                 Case Is <= Format(Now, "ee")
                     y = Left(strVal, 2) + HEISEI
                 
             End Select
             
             res = y & "/" & Right(strVal, 2) & "/01"
             res = Format(res, "gggee年mm月")

        Case 6:
            'yyyymm 201612
            If Left(strVal, 2) = "20" Or Left(strVal, 2) = "19" Then
                res = strVal & "01"
                res = Left(strVal, 4) & "/" & Mid(strVal, 5, 2) & "/" & Right(strVal, 2)
                res = Format(res, "ggge年mm月")
                
            Else
            'ggmmdd 281010
                Select Case CInt(Left(strVal, 2))
                    '昭和
                    Case Is > Format(Now, "ee") + 1
                        y = Left(strVal, 2) + SHOWA
                    
                    '平成
                    Case Is <= Format(Now, "ee")
                        y = Left(strVal, 2) + HEISEI
                    
                End Select
                res = y & "/" & Mid(strVal, 3, 2) & "/" & Right(strVal, 2)
                res = Format(res, "ggge年mm月dd日")
                
            End If
            
        Case 8:
            'yyyymmdd 20161223
            res = Left(strVal, 4) & "/" & Mid(strVal, 5, 2) & "/" & Right(strVal, 2)
            res = Format(res, "ggge年mm月dd日")
        
        Case 7
            '4281011
            '日まで
            res = strWareki & Mid(strVal, 2, 2) & "年" & Mid(strVal, 4, 2) & "月" & Right(strVal, 2) & "日"
        
        Case 5
            '42810
            '年月のみ
            res = strWareki & Mid(strVal, 2, 2) & "年" & Mid(strVal, 4, 2) & "月"
                        
        '//20170714174502 furukawa st ////////////////////////
        '//桁数が想定外の場合はそのまま出す
        Case Else
            res = strVal
        '//20170714174502 furukawa ed ////////////////////////
    End Select
    
    
    If Not IsDate(res) Then
        ChangeDateFormat = "エラー"
        Exit Function
    End If
    
    ChangeDateFormat = res
        

End Function

