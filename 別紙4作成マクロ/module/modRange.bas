Attribute VB_Name = "modRange"
Option Explicit

'y[WæØès@ÊQ
Public Enum PageBottomLine
    page1 = 61
    page2 = 120
    page3 = 179
    page4 = 238
    page5 = 297
End Enum

'Zì¬V[gÌñ@ÊQ
Public Enum ReceCol
    ReceColWidth2 = 16
    ReceColWidth3 = 47
    ReceColWidth4 = 7
    ReceColWidth5 = 5
    ReceColWidth6 = 8.25
    ReceColWidth7 = 5
End Enum


'ZÍÍÏ@ÊSp
Public rngKoban_Bessi4          As Range 'Ô
Public rngEdaban_Bessi4         As Range '}Ô
Public rngPageCount_Bessi4      As Range 'y[W
Public rngSanteiKanou_Bessi4    As Range 'Zèóµ
Public rngSinseiKubun_Bessi4    As Range '\¿æª
Public rngSinryoKoui_Bessi4     As Range 'fÃs×
Public rngIyakuhin_Bessi4       As Range 'ãòi
Public rngTokki_Bessi4          As Range 'ÁL


'ZÍÍ@ÊSp
Public Const cellsKoban_Bessi4 = "C17"                 'Ô
Public Const cellsEdaban_Bessi4 = "F17"                '}Ô
Public Const cellsPageCount_Bessi4 = "H25"             'y[W
Public Const cellsSanteiKanou_Bessi4 = "P25"           'Zèóµ
Public Const cellsSinseiKubun_Bessi4 = "M28"           '\¿æª
Public Const cellsSinryoKoui_Bessi4 = "F34"            'fÃs×
Public Const cellsIyakuhin_Bessi4 = "F44"              'ãòi
Public Const cellsTokki_Bessi4 = "C54"                 'ÁL



'ZÍÍÏ@ÊQp
Public rngSinryoSikibetuHyo As Range        'fÃ¯ÊXgÌÍÍ
Public rngPage1             As Range        'y[W1ÌüÍÍÍ
Public rngPage2             As Range        'y[W2ÌüÍÍÍ
Public rngPage3             As Range        'y[W3ÌüÍÍÍ
Public rngPage4             As Range        'y[W4ÌüÍÍÍ
Public rngPage5             As Range        'y[W5ÌüÍÍÍ
Public rngBiko              As Range        'õl
Public rngSum               As Range        'v_
Public rngKomoku            As Range        'üÍÍÍ

Public rngSinryoSikibetuCol As Range        'fÃ¯Êñ
Public rngManage            As Range        'ÇîñÍÍ
Public rngKojin             As Range        'ÂlîñÍÍ
Public rngSyobyoIryokikan   As Range        'a¼ãÃ@Ö¼
Public rngSinryoSikibetu    As Range        'fÃ¯ÊÌÍÍ

Public rngTitle             As Range        '^Cg
Public rngVer               As Range        'o[W


'ZÍÍiÈjÊQ
Public Const cellsPage1_Sika = "$b$16:$e$53,$g$16:$g$53"                              'y[W1ÌüÍÍÍÈ
Public Const cellsPage2_Sika = "$b$64:$e$111,$g$64:$g$111"                            'y[W2ÌüÍÍÍÈ
Public Const cellsPage3_Sika = "$b$123:$e$170,$g$123:$g$170"                          'y[W3ÌüÍÍÍÈ
Public Const cellsPage4_Sika = "$b$182:$e$229,$g$182:$g$229"                          'y[W4ÌüÍÍÍÈ
Public Const cellsPage5_Sika = "$b$241:$e$288,$g$241:$g$288"                          'y[W5ÌüÍÍÍÈ

Public Const cellsSinryoSikibetuCol_Sika = _
"$b$16:$b$54,b$64:b$112,$b$123:$b$171,$b$182:$b$230,$b$241:$b$289"                    'fÃ¯ÊñÈ

'//20180910134208 furukawa st ////////////////////////
'//ÈÌfÃ¯Ê\ðãÈÆ¯¶êÉµ½
'Public Const cellsSinryoSikibetuHyo_Sika = "$b$301:$b$314"                            'fÃ¯ÊXgÌÍÍÈ
Public Const cellsSinryoSikibetuHyo_Sika = "$j$44:$j$57"                              'fÃ¯ÊXgÌÍÍãÈ
'//20180910134208 furukawa ed ////////////////////////

Public Const cellsBiko_Sika = "$b$55,$b$113,$b$172,$b$231,$b$290"                     'õlÈ
Public Const cellsSum_Sika = "$e$54,$e$112,$e$171,$e$230,$e$289"                      'v_È
Public Const cellsKomoku_Sika = _
    "$c$16:$c$53,c$64:c$111,$c$123:$c$170,$c$182:$c$229,$c$241:$c$288"                'üÍÍÍÈ




'ZÍÍiãÈjÊQ
'//20180907162652 furukawa st ////////////////////////
'//ÊQCAEgÏXÉº¤ÏX
'Public Const cellsPage1_Ika = "$b$16:$e$53,$g$16:$g$53"                              'y[W1ÌüÍÍÍãÈ
Public Const cellsPage1_Ika = "$b$16:$e$59,$g$16:$g$59"                              'y[W1ÌüÍÍÍãÈ
'//20180907162652 furukawa ed ////////////////////////

Public Const cellsPage2_Ika = "$b$64:$e$119,$g$64:$g$119"                            'y[W2ÌüÍÍÍãÈ
Public Const cellsPage3_Ika = "$b$123:$e$178,$g$123:$g$178"                          'y[W3ÌüÍÍÍãÈ
Public Const cellsPage4_Ika = "$b$182:$e$237,$g$182:$g$237"                          'y[W4ÌüÍÍÍãÈ
Public Const cellsPage5_Ika = "$b$241:$e$296,$g$241:$g$296"                          'y[W5ÌüÍÍÍãÈ


Public Const cellsSinryoSikibetuHyo_Ika = "$j$44:$j$57"                              'fÃ¯ÊXgÌÍÍãÈ


'//20180907155006 furukawa st ////////////////////////
'//ÊQCAEgÏXÉº¤ÏX

'
'Public Const cellsSinryoSikibetuCol_Ika = _
'    "$b$16:$b$53,b$64:b$119,$b$123:$b$178,$b$182:$b$237,$b$241:$b$296"               'fÃ¯ÊñãÈ
'Public Const cellsBiko_Ika = "$b$55"                                                 'õlãÈ
'Public Const cellsSum_Ika = "$e$54"                                                  'v_ãÈ
'Public Const cellsKomoku_Ika = _
'    "$c$16:$c$53,c$64:c$119,$c$123:$c$178,$c$182:$c$237,$c$241:$c$296"               'üÍÍÍãÈ
'

Public Const cellsSinryoSikibetuCol_Ika = _
    "$b$16:$b$59,b$64:b$119,$b$123:$b$178,$b$182:$b$237,$b$241:$b$296"               'fÃ¯ÊñãÈ
Public Const cellsBiko_Ika = "$C7:$V53"                                              'õlãÈÊV[g
Public Const cellsSum_Ika = "$e$60"                                                  'v_ãÈ
Public Const cellsKomoku_Ika = _
    "$c$16:$c$59,c$64:c$119,$c$123:$c$178,$c$182:$c$237,$c$241:$c$296"               'üÍÍÍãÈ


'//20180907155006 furukawa ed ////////////////////////



'ZÍÍiãÈÈjÊQ
'//20180830144511 furukawa st ////////////////////////
'//cellsManageÉfÃúÔI¹ðÜß½
Public Const cellsManage = "$j$2,$m$2,$j$4,$j$6,$L$7,$K$6"                            'ÇîñÍÍ
'Public Const cellsManage = "$j$2,$m$2,$j$4,$j$6,$L$7"                            'ÇîñÍÍ
'//20180830144511 furukawa ed ////////////////////////

Public Const cellsKojin = "$c$6:$c$10,$f$7,$e$10,$c$12,$G$13"                    'ÂlîñÍÍ


Public Const cellsTitle = "B3"                                                   '^Cg

'//20181119113510 furukawa st ////////////////////////
'//o[WÍÍðgå
'Public Const cellsVer = "Y15:Y70"                                                'o[WÍÍ
Public Const cellsVer = "Y15:Y120"                                                'o[WÍÍ
'//20181119113510 furukawa ed ////////////////////////
    
    
'K{`FbNÚZÍÍÏ@ÊQ
Public rngKoban As Range                  'Ô
Public rngEdaban As Range                 '}Ô

Public rngSinryoKikanST As Range          'fÃúÔJn
Public rngSinryoKikanED As Range          'fÃúÔI¹
Public rngSakuseisya As Range             'ì¬Ò

Public rngSinryoYM As Range               'fÃN
Public rngSyobyo As Range                 'a¼
Public rngNissu As Range                  'Àú

Public rngBirthDay As Range               '¶Nú
Public rngIryokikan As Range              'ãÃ@Ö¼


'K{`FbNÚÍÍ@ÊQ
Public Const cellsKoban = "j2"              'Ô
Public Const cellsEdaban = "m2"             '}Ô
Public Const cellsItakuMaisu = "j4"         'Ïõ

Public Const cellsSinryoKikanST = "j6"      'fÃúÔJn
Public Const cellsSinryoKikanED = "k6"      'fÃúÔI¹
Public Const cellsKensu = "l7"              'ì¬
Public Const cellsSakuseisya = "j9"         'ì¬Ò

Public Const cellsSinryoYM = "f7"           'fÃN
Public Const cellsSyobyo = "c12"            'a¼
Public Const cellsNissu = "g13"             'Àú


Public Const cellsBirthday = "C10"          '¶Nú
Public Const cellsIryokikan = "E10"         'ãÃ@Ö¼

'¤Ê
Public Const Common_Row_Height = 18                             '3sÚ³
Public Const Common_Font_Name = "lr oSVbN"               'tHg¼
Public Const Common_Protect_Password = "2021idem"               'ÛìpX[h


'¤ÊÍÍ
Private Sub modRange_RngCommon(ByVal ws As Worksheet)
    Set rngSinryoKikanST = ws.Range(cellsSinryoKikanST)                     'fÃúÔJn
    Set rngSinryoKikanED = ws.Range(cellsSinryoKikanED)                     'fÃúÔI¹
    
    Set rngSinryoSikibetuCol = ws.Range(cellsSinryoSikibetuCol_Sika)        'fÃ¯ÊüÍÍÍ
    Set rngSinryoSikibetuHyo = ws.Range(cellsSinryoSikibetuHyo_Sika)        'fÃ¯Ê\ÌÍÍ
    Set rngManage = ws.Range(cellsManage)                                   'ÇîñÍÍ
    Set rngKojin = ws.Range(cellsKojin)                                     'ÂlîñÍÍ
    
    
    Set rngKoban = ws.Range(cellsKoban)                        'Ô
    Set rngEdaban = ws.Range(cellsEdaban)                      '}Ô
    Set rngSinryoKikanST = ws.Range(cellsSinryoKikanST)        'fÃúÔJn
    Set rngSinryoKikanED = ws.Range(cellsSinryoKikanED)        'fÃúÔI¹
    Set rngSakuseisya = ws.Range(cellsSakuseisya)              'ì¬Ò
    Set rngSinryoYM = ws.Range(cellsSinryoYM)                  'fÃN
    Set rngSyobyo = ws.Range(cellsSyobyo)                      'a¼
    Set rngNissu = ws.Range(cellsNissu)                        'Àú
    Set rngBirthDay = ws.Range(cellsBirthday)                  '¶Nú
    Set rngIryokikan = ws.Range(cellsIryokikan)                'ãÃ@Ö¼
    
    Set rngTitle = ws.Range(cellsTitle)                         '^Cg
    
    Set rngVer = ws.Range(cellsVer)                             'o[W
    

End Sub

'ÈpÍÍwè
Public Sub modRange_RngSika(ByVal ws As Worksheet)
               
    Set rngSinryoSikibetuCol = ws.Range(cellsSinryoSikibetuCol_Sika)         'fÃ¯ÊñÈ
    Set rngSinryoSikibetuHyo = ws.Range(cellsSinryoSikibetuHyo_Sika)         'fÃ¯Ê\È
    
    Set rngPage1 = ws.Range(cellsPage1_Sika) 'y[W1ÌüÍÍÍÈ
    Set rngPage2 = ws.Range(cellsPage2_Sika) 'y[W2ÌüÍÍÍÈ
    Set rngPage3 = ws.Range(cellsPage3_Sika) 'y[W3ÌüÍÍÍÈ
    Set rngPage4 = ws.Range(cellsPage4_Sika) 'y[W4ÌüÍÍÍÈ
    Set rngPage5 = ws.Range(cellsPage5_Sika) 'y[W5ÌüÍÍÍÈ

    Set rngBiko = ws.Range(cellsBiko_Sika)                              'õlÌÍÍÈ
    Set rngSum = ws.Range(cellsSum_Sika)                                'v_È
    Set rngKomoku = ws.Range(cellsKomoku_Sika)                          'üÍÍÍÈ
    
    
    Call modRange_RngCommon(ws) '¤ÊÚ
End Sub


'ãÈpÍÍ
Public Sub modRange_RngIka(ByVal ws As Worksheet)

    Set rngSinryoSikibetuCol = ws.Range(cellsSinryoSikibetuCol_Ika)         'fÃ¯ÊñãÈ
    Set rngSinryoSikibetuHyo = ws.Range(cellsSinryoSikibetuHyo_Ika)         'fÃ¯Ê\ãÈ
    
    Set rngPage1 = ws.Range(cellsPage1_Ika) 'y[W1ÌüÍÍÍãÈ
    Set rngPage2 = ws.Range(cellsPage2_Ika) 'y[W2ÌüÍÍÍãÈ
    Set rngPage3 = ws.Range(cellsPage3_Ika) 'y[W3ÌüÍÍÍãÈ
    Set rngPage4 = ws.Range(cellsPage4_Ika) 'y[W4ÌüÍÍÍãÈ
    Set rngPage5 = ws.Range(cellsPage5_Ika) 'y[W5ÌüÍÍÍãÈ

    'Set rngBiko = ws.Range(cellsBiko_Ika)                              'õlÌÍÍãÈ
    Set rngSum = ws.Range(cellsSum_Ika)                                'v_ãÈ
    Set rngKomoku = ws.Range(cellsKomoku_Ika)                          'üÍÍÍãÈ
    
    
    Call modRange_RngCommon(ws) '¤ÊÚ


End Sub


'ÊSÍÍ
Public Sub modRange_RngBessi4(ByVal ws As Worksheet)

    Set rngKoban_Bessi4 = ws.Range(cellsKoban_Bessi4)                   'Ô
    Set rngEdaban_Bessi4 = ws.Range(cellsEdaban_Bessi4)                 '}Ô
    Set rngPageCount_Bessi4 = ws.Range(cellsPageCount_Bessi4)           'y[W
    Set rngSanteiKanou_Bessi4 = ws.Range(cellsSanteiKanou_Bessi4)       'Zèóµ
    Set rngSinseiKubun_Bessi4 = ws.Range(cellsSinseiKubun_Bessi4)       '\¿æª
    Set rngSinryoKoui_Bessi4 = ws.Range(cellsSinryoKoui_Bessi4)         'fÃs×
    Set rngIyakuhin_Bessi4 = ws.Range(cellsIyakuhin_Bessi4)             'ãòi
    Set rngTokki_Bessi4 = ws.Range(cellsTokki_Bessi4)                   'ÁL

End Sub


'õly[Wè`
Public Sub modRange_RngBiko(ByVal ws As Worksheet)
    Set rngBiko = ws.Range(cellsBiko_Ika)
    
End Sub


'o[W
Public Sub modRange_Ver(ByVal ws As Worksheet)
    
    Dim r As Long
    
    For r = rngVer.Rows.Count To 1 Step -1
        If Cells(r, rngVer.Column).Value <> "" Then
            Dim strV As String
            strV = Cells(r, rngVer.Column).Value
            ws.Range("J15") = "Ver." & strV
            Exit For
        End If
    Next
    
End Sub
