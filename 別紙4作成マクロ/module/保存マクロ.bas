Attribute VB_Name = "保存マクロ"
Option Explicit

Dim global_strBiko As Variant

Dim arrSinryoSikibetu() As String
Dim arrDrug() As String

Dim ex As Excel.Application
Dim wb As Workbook
        
'使ったページ数
Dim needpage As Integer

'チェッククラス
Dim clsChk As New clsChk

Sub main()



    
    '範囲定義取得 医科
    Call modRange_RngIka(ThisWorkbook.Worksheets(2))
    
    '//20180907162338 furukawa st ////////////////////////
    '//備考ページ範囲定義取得
    Call modRange_RngBiko(ThisWorkbook.Worksheets("備考"))
    '//20180907162338 furukawa ed ////////////////////////
        
    'バージョン取得表示
    modRange_Ver ThisWorkbook.Worksheets(2)

    'セル書式変更
    CellFormatChange
    
    '保存前チェック
    If Not clsChk.ChkMain(ThisWorkbook.Worksheets(2)) Then Exit Sub
    
    
    
    '新規ブックにコピー
    Copyworksheets
       
    '別紙4表示用　診療識別の取得
    GetSinryoSikibetu
    
    '別紙4表示用　薬剤取得
    GetDrug
    
    '使用範囲の特定、無駄な部分を削る + 保護再開
    ChkUsedRange
    
    '別紙４作成
    Create_Bessi4
      
    '保存
    SaveWorkSheet

    Application.DisplayAlerts = True
    
    Set clsChk = Nothing
    
End Sub

Private Sub errproc(strProcName As String)
    MsgBox strProcName & vbCrLf & err.Description, vbCritical
    
End Sub


'//20170607153709 furukawa st ////////////////////////
Private Function IsSika() As Boolean

'//20170712154419 furukawa st ////////////////////////
'//大文字小文字の区別ができなかった
    'If Right(ThisWorkbook.Worksheets(2).rngkoban, 1) = "c" Then
    
    
    'ファイル名に歯科があれば歯科
    If InStr(1, ThisWorkbook.Name, "歯科") > 0 Then
        IsSika = True
        
        
    ElseIf Not rngKoban Is Nothing Then
        If (Right(rngKoban, 1) = "c" Or Right(rngKoban, 1) = "C") Or _
        (InStr(1, ThisWorkbook.Name, "歯科") > 0) Then
        
'        If Right(rngKoban, 1) = "c" Or _
'        Right(rngKoban, 1) = "C" Then
        
'//20170712154419 furukawa ed ////////////////////////

        IsSika = True
    End If
    
    Else
        IsSika = False
    End If
    
End Function


Private Sub Copyworksheets()
On Error GoTo err

    '両シートともコピー
    Dim arrsheet As Variant
    
    
    
    '//20180907144800 furukawa st ////////////////////////
    '//備考ページもコピーする
    arrsheet = Array(ThisWorkbook.Worksheets(1).Name, ThisWorkbook.Worksheets(2).Name, ThisWorkbook.Worksheets("備考").Name)
    'arrsheet = Array(ThisWorkbook.Worksheets(1).Name, ThisWorkbook.Worksheets(2).Name)
    '//20180907144800 furukawa ed ////////////////////////
    
    ThisWorkbook.Worksheets(arrsheet).Copy
    
    'コピー先のブック
    Set wb = ActiveWorkbook
    With wb.Sheets(2)
    
        .Unprotect password:=Common_Protect_Password
        'ボタン削除
        .Buttons.Delete
        
        'フォント統一
        .Cells.Font.Name = Common_Font_Name
        '3行目高さ
        .Rows(3).RowHeight = Common_Row_Height
        
        '列幅指定
        .Columns(2).ColumnWidth = ReceCol.ReceColWidth2
        .Columns(3).ColumnWidth = ReceCol.ReceColWidth3
        .Columns(4).ColumnWidth = ReceCol.ReceColWidth4
        .Columns(5).ColumnWidth = ReceCol.ReceColWidth5
        .Columns(6).ColumnWidth = ReceCol.ReceColWidth6
        .Columns(7).ColumnWidth = ReceCol.ReceColWidth7
        
        rngSinryoKikanST.NumberFormatLocal = "@"
        rngSinryoKikanED.NumberFormatLocal = "@"
        
        '//20170704163728 furukawa st ////////////////////////
        '//診療期間の入力規則解除。修正時に入力できない
        rngSinryoKikanST.Validation.Delete
        rngSinryoKikanED.Validation.Delete
        '//20170704163728 furukawa ed ////////////////////////


        '//20180910105919 furukawa st ////////////////////////
        '//変更履歴削除範囲を拡大
        .Columns("ac:ac").Delete
        .Columns("ab:ab").Delete
        .Columns("aa:aa").Delete
        '//20180910105919 furukawa ed ////////////////////////
        
        '変更履歴削除
        .Columns("z:z").Delete
        .Columns("y:y").Delete
        .Columns("x:x").Delete
        
        '西暦和暦換算表削除
        .Range("L12:U39").Delete
        
    End With
    
    
    '//20181221161807 furukawa st ////////////////////////
    '//備考入力のボタンを削除
    With wb.Worksheets("備考")
        .btn.Visible = False
    End With
    '//20181221161807 furukawa ed ////////////////////////
    
    '//20180914115954 furukawa st ////////////////////////
    '//備考ページの色を解除
    With wb.Worksheets("備考")
        .Range("C7:V53").Interior.Color = xlNone
    End With
    '//20180914115954 furukawa ed ////////////////////////
    
    Exit Sub
err:
    Call errproc("Copyworksheets")
End Sub

Private Function SaveWorkSheet() As Boolean
On Error GoTo err

    '//20170712142013 furukawa st ////////////////////////
    '//ファイル名をハイフン付きに変換

    Dim strpath As String
    Dim filename As String
    
    '枝番有無でファイル名変更
    If rngEdaban.Value <> "" Then
        filename = StrConv(StrConv(rngKoban.Value & "-" & _
                rngEdaban, vbNarrow), vbUpperCase) & ".xlsx"
    Else
        filename = StrConv(StrConv(rngKoban.Value, vbNarrow), vbUpperCase) & ".xlsx"
    End If
    
    filename = Left(filename, 6) & "-" & Mid(filename, 7, 3) & "-" & Mid(filename, 10)
    strpath = ThisWorkbook.Path & "\" & filename
    '//20170712142013 furukawa ed ////////////////////////


    '//20181221095209 furukawa st ////////////////////////
    '//displayalertsをfalseにしたから警告でないが、出さないとユーザが上書きしてしまう可能性がある
    Dim fso As Object
    Set fso = CreateObject("Scripting.FilesystemObject")
    If fso.FileExists(strpath) Then
        If MsgBox("同じ名前のファイルが存在します。上書きしますか？", vbYesNo + vbQuestion) = vbNo Then
            SaveWorkSheet = True
            Set fso = Nothing
            Exit Function
        End If
    End If
    '//20181221095209 furukawa ed ////////////////////////
                        
                        
    '//20170601143705 furukawa st ////////////////////////
    '//原本の別紙４は非表示、完成版は表示させる
    Dim cnt As Integer
    For cnt = 1 To wb.Worksheets.Count
        wb.Sheets(cnt).Visible = True
    Next
    '//20170601143705 furukawa ed ////////////////////////

    On Error Resume Next
    
    '//20181221093158 furukawa st ////////////////////////
    '//マクロブック保存関連警告の非表示
    Application.DisplayAlerts = False
    '//20181221093158 furukawa ed ////////////////////////

    wb.SaveAs filename:=strpath, FileFormat:=xlOpenXMLWorkbook
    If err.Number <> 0 Then
        'wb.Close False
        Exit Function
    End If
    
    SaveWorkSheet = True

    Exit Function
err:
    Call errproc("SaveWorkSheet")
    
    '//20170714091546 furukawa st ////////////////////////
    '//DisplayAlartを消してみる（excel落ちる対策）
    'Application.DisplayAlerts = True
    '//20170714091546 furukawa ed ////////////////////////
    
End Function

Public Sub Initialize()
On Error GoTo err

    
    Set wb = ActiveWorkbook
    
    With wb.Worksheets(2)
        
        '範囲取得　医科
        If IsSika Then
            Call modRange_RngSika(wb.Worksheets(2))
        Else
            Call modRange_RngIka(wb.Worksheets(2))
        End If
        
        
        '個人情報を流用する場合
        If MsgBox("個人情報を削除しますか？", _
            vbQuestion + vbYesNo + vbDefaultButton2) = vbYes Then
                
            'セルの初期化
            rngManage.Value = ""
            rngKojin.Value = ""
        
        End If
        
        If MsgBox("入力したデータを全て削除しますか？", _
            vbQuestion + vbYesNo + vbDefaultButton2) = vbNo Then Exit Sub
        
        
        needpage = 5
        Erase arrSinryoSikibetu
        Erase arrDrug
        
        rngPage1.ClearContents
        rngPage2.ClearContents
        rngPage3.ClearContents
        rngPage4.ClearContents
        rngPage5.ClearContents
        
        '//20170706120123 furukawa st ////////////////////////
        '//診療年月、傷病名、実日数は削除（歯科以外）
        If Not IsSika Then
            rngSinryoYM = ""
            rngSyobyo = ""
            rngNissu = ""
        End If
        '//20170706120123 furukawa ed ////////////////////////
        
        
        'ほみそエラーの解除
        Call clsChk.RemoveHomisoError(wb.Worksheets(2))
        '診療識別列エラー解除
        Call clsChk.RemoveSinryoSikibetuErr(wb.Worksheets(2))
                
                
        '//20180910094713 furukawa st ////////////////////////
        '//備考欄変更に伴い削除
        'rngBiko = "【備考】"
        '//20180910094713 furukawa ed ////////////////////////
        
        '診療識別表の入力規則
        Dim st As String
        If Not IsSika Then
            st = ActiveSheet.Name & "!" & cellsSinryoSikibetuHyo_Ika '←医科用
        Else
            st = ActiveSheet.Name & "!" & cellsSinryoSikibetuHyo_Sika '←歯科用
        End If
        
        '入力規則は保護解除が必要
        .Unprotect password:=Common_Protect_Password
        rngSinryoSikibetuCol.Validation.Delete
        rngSinryoSikibetuCol.Validation.Add xlValidateList, , xlEqual, "=" & st
        .Protect password:=Common_Protect_Password, contents:=True, AllowFormattingCells:=True
        
        rngEdaban.Interior.ColorIndex = 0
        rngSakuseisya.Interior.ColorIndex = 0
        rngKoban.Interior.ColorIndex = 0
        rngSinryoKikanST.Interior.ColorIndex = 0
    End With
    
    
    '//20180910093711 furukawa st ////////////////////////
    '//備考ページのクリア
    With wb.Worksheets("備考")
        .Range(cellsBiko_Ika).ClearContents
    End With
    '//20180910093711 furukawa ed ////////////////////////
    
    
    
    '//20170602140945 furukawa st ////////////////////////
    '//点数は関数で行うのでセル内削除しない
'    With wb.Worksheets(1)
'        .Unprotect password:=common_protect_password
'         '点数も削除
'        .Range("f49") = 0
'        .Protect password:=common_protect_password, contents:=True, AllowFormattingCells:=True
'    End With
    '//20170602140945 furukawa ed ////////////////////////
    
    Exit Sub
err:
    Call errproc("Initialize")
End Sub


'別紙4に表示するために診療識別を取得
Private Sub GetSinryoSikibetu()
On Error GoTo err

    Dim strSinryoSikibetu As Variant
    Dim r As Integer
    
    '入力されている診療識別を取得
    For Each strSinryoSikibetu In rngSinryoSikibetuCol
        If strSinryoSikibetu <> "" Then
            ReDim Preserve arrSinryoSikibetu(r)
            
            arrSinryoSikibetu(r) = strSinryoSikibetu
            r = r + 1
        End If
    Next
    
    
    Exit Sub
err:
    Call errproc("GetSinryoSikibetu")

End Sub


'別紙4に表示するために医薬品を取得
Private Sub GetDrug()
On Error GoTo err

    
    Dim r As Integer
    Dim rng As Range
    
    Dim strToyaku As String
    Dim arrRng() As Range
    Dim arrCnt As Long
    Dim cnt As Long
    Dim cntRow As Long
    
    If IsSika Then
        '歯科の場合
        strToyaku = "(21) 投薬・注射"
        Set rng = rngSinryoSikibetuCol.Find("(21) 投薬・注射")
    Else
        '医科の場合
        strToyaku = "(20) 投薬"
        Set rng = rngSinryoSikibetuCol.Find("(20) 投薬")
    End If
    
    
    '//20190104165042 furukawa st ////////////////////////
    '//投薬は1レセ1ではないので、指定範囲の全部取得する
    For Each rng In rngSinryoSikibetuCol
        If rng.Value = strToyaku Then
                    
            Dim rng2 As Range
            Set rng2 = rng.End(xlDown)
            
            
            '入力されている医薬品名を取得
            cntRow = 0
            For r = rng.Row To rng2.Row
                If rng.Offset(cntRow, 1) <> "" Then
                    ReDim Preserve arrDrug(cnt)
                    arrDrug(cnt) = rng.Offset(cntRow, 1)
                    cnt = cnt + 1
                    cntRow = cntRow + 1
                End If
            Next
            
        End If
    Next
'//20190104165042 furukawa ed ////////////////////////

    Exit Sub
err:
    Call errproc("GetDrug")

End Sub


'必要なページを残して他は削除
Private Function ChkUsedRange() As Range
On Error GoTo err
    With wb.Worksheets(2)
        Dim rng As Range
        
        'ページ内に値があればそのページは使用する
        For Each rng In rngPage1
            If rng.Value <> "" Then needpage = 1: Exit For
        Next
        For Each rng In rngPage2
            If rng.Value <> "" Then needpage = 2: Exit For
        Next
        For Each rng In rngPage3
            If rng.Value <> "" Then needpage = 3: Exit For
        Next
        For Each rng In rngPage4
            If rng.Value <> "" Then needpage = 4: Exit For
        Next
        For Each rng In rngPage5
            If rng.Value <> "" Then needpage = 5: Exit For
        Next
        
        '不要ページの削除
        Select Case needpage
            Case 1: .Rows(page1 + 1 & ":" & page5).Delete
            Case 2: .Rows(page2 + 1 & ":" & page5).Delete
            Case 3: .Rows(page3 + 1 & ":" & page5).Delete
            Case 4: .Rows(page4 + 1 & ":" & page5).Delete
        End Select
        
        '保護再開
        .Protect password:=Common_Protect_Password, contents:=True, AllowFormattingCells:=True

    End With
            
    Exit Function
err:
    Call errproc("ChkUsedRange")


End Function

'//20170607133958 furukawa st ////////////////////////
'備考作成
Private Function CreateBiko() As String
    Dim res As String
    Dim tmprange As Range
    
    For Each tmprange In rngBiko
    'For Each tmprange In rngBiko
        If tmprange.Value <> "" Then
            res = res & Replace(tmprange.Value, "【備考】", "") & vbCrLf
        End If
    Next
    
    CreateBiko = res
    
End Function

'診療科区別
Private Function SinryoKaHantei(strVal As String) As String
On Error GoTo err
    Dim res As String
    
    Select Case strVal
        Case "A": res = "医科入院"
        Case "B": res = "医科外来"
        Case "C": res = "歯科"
        Case Else: res = ""
    End Select
    
    SinryoKaHantei = res
    
    Exit Function
err:
    Call errproc("SinryoKaHantei")

End Function

'別紙４作成
Private Sub Create_Bessi4()
On Error GoTo err

    Call modRange_RngBessi4(wb.Worksheets(1))
    
    With wb.Worksheets(1)
        .Unprotect password:=Common_Protect_Password


        '項番
        rngKoban_Bessi4 = StrConv(rngKoban, vbNarrow)
        
        '枝番
        If rngEdaban_Bessi4 <> "" Then rngEdaban_Bessi4 = StrConv(rngEdaban, vbNarrow)
      
        '算定可不可
        rngSanteiKanou_Bessi4 = "算定可能"
    
        '当レセプト作成枚数をページ数にする
        rngPageCount_Bessi4 = needpage
        
        '区別
        rngSinseiKubun_Bessi4 = SinryoKaHantei(Right(StrConv(rngKoban, vbNarrow), 1))
       
            
        '診療期間
        Dim strst As String
        Dim stred As String
        strst = rngSinryoKikanST
        stred = rngSinryoKikanED
        
        '分類
        Dim strSinryoSikibetu As String
        Dim r As Integer
        Dim flg As Boolean
        
        '診療識別の表示
        On Error Resume Next
        For r = 0 To UBound(arrSinryoSikibetu)
            strSinryoSikibetu = strSinryoSikibetu & arrSinryoSikibetu(r) & vbCrLf
        Next
        rngSinryoKoui_Bessi4 = strSinryoSikibetu
    
        
        '医薬品の表示
        Dim strdrug As String
        For r = 0 To UBound(arrDrug)
            strdrug = strdrug & arrDrug(r) & "、"
        Next
        strdrug = Left(strdrug, Len(strdrug) - 1)
        rngIyakuhin_Bessi4 = strdrug
    
    
    
    
    
        '//20190104165750 furukawa st ////////////////////////
        '//医薬品文字列が長い場合サイズを6にする
        If Len(strdrug) > 200 Then
            rngIyakuhin_Bessi4.Font.Size = 6
        Else
            rngIyakuhin_Bessi4.Font.Size = 11
        End If
        
        '//20190104165750 furukawa ed ////////////////////////
    
    
        
        
    
    
        '//20170609163015 furukawa st ////////////////////////
        '//医科歯科とも備考作成
        '備考作成
        rngTokki_Bessi4 = CreateBiko
        
        '//20170609163015 furukawa ed ////////////////////////
        
        
        '//20180907171048 furukawa st ////////////////////////
        '//備考が無い場合はシートごと削除、あったら備考参照
        Application.DisplayAlerts = False
        If rngTokki_Bessi4 = "" Then
            wb.Sheets("備考").Delete
        Else
            rngTokki_Bessi4 = "備考ページ参照"
        End If
        Application.DisplayAlerts = True
        '//20180907171048 furukawa ed ////////////////////////
                
        
        '//20170607144723 furukawa st ////////////////////////
        '//フォントをMSPゴシックに統一（excelバージョンによって勝手に置き換わり印刷範囲がずれる）
        
        '//20180621115028 furukawa st ////////////////////////
        '//フォントをMSゴシックにする（見やすく）
        '.Cells.Font.Name = "ｍｓ ｐゴシック"
        .Cells.Font.Name = "ｍｓ ゴシック"
        '//20180621115028 furukawa ed ////////////////////////
        
        '//20170607144723 furukawa ed ////////////////////////
        
        On Error GoTo err
        
        '//20170706093519 furukawa st ////////////////////////
        '//別紙4は保護しない（山内）
        '.Protect password:=common_protect_password, contents:=True, AllowFormattingCells:=True
        '//20170706093519 furukawa ed ////////////////////////
        
    End With
    
    Exit Sub
err:
    Call errproc("Create_Bessi4")


End Sub

Private Sub CellFormatChange()
On Error GoTo err
    
    '項番 半角大文字に
    rngKoban.Value = StrConv(rngKoban.Value, vbNarrow + vbUpperCase)
    
    '医科の場合は診療年月を平成〜に変える
    If Not IsSika Then rngSinryoYM.Value = clsChk.ChangeDateFormat(rngSinryoYM.Value) '診療年月
    '//20170706121515 furukawa ed ////////////////////////
                
    rngSinryoKikanED.Value = clsChk.ChangeDateFormat(rngSinryoKikanED.Value) '診療期間終了
    rngSinryoKikanST.Value = clsChk.ChangeDateFormat(rngSinryoKikanST.Value) '診療期間開始

    If Not IsSika Then rngBirthDay.Value = clsChk.ChangeDateFormat(rngBirthDay.Value) '生年月日
    
    '診療科判定
    rngTitle = "海外療養費保険適用計算書/" & SinryoKaHantei(StrConv(Right(rngKoban.Value, 1), vbNarrow + vbUpperCase))
    
    '//20170607144906 furukawa st ////////////////////////
    '//改行の削除（印刷範囲がずれる）
    Dim rng As Range
    For Each rng In rngKomoku
        If InStr(rng.Value, vbCrLf) > 0 Then rng = Replace(rng.Value, vbCrLf, "")
    Next
    '//20170607144906 furukawa ed ////////////////////////


    
    Exit Sub
err:
    Call errproc("CellFormatChange")

End Sub

