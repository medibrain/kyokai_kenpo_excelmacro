Attribute VB_Name = "保存マクロ歯科"
Option Explicit

Dim arrSinryoSikibetu() As String
Dim arrDrug() As String

Dim ex As Excel.Application
Dim wb As Workbook
        
'使ったページ数
Dim needpage As Integer

'チェッククラス
Dim clsChk As New clsChk

Sub main()
    
    '範囲定義取得
    Call modRange_RngSika(ThisWorkbook.Worksheets(2))
    
    '保存前チェック
    If Not clsChk.ChkMain(ThisWorkbook.Worksheets(2)) Then Exit Sub
    
    'バージョン取得表示
    modRange_Ver ThisWorkbook.Worksheets(2)

    'まず新規ブックにコピー
    Copyworksheets
       
    '診療識別の取得
    GetSinryoSikibetu
    
    '薬剤取得
    GetDrug
    
    'セル書式変更
    CellFormatChange
    
    '使用範囲の特定、無駄な部分を削る + 保護再開
    ChkUsedRange
    
    '別紙４作成
    Create_Bessi4
      
    '保存
    SaveWorkSheet

    Set clsChk = Nothing
    
End Sub

Private Sub errproc(strProcName As String)
    MsgBox strProcName & vbCrLf & err.Description, vbCritical
    
End Sub


'//20170607153709 furukawa st ////////////////////////
Private Function IsSika() As Boolean

'//20170712154419 furukawa st ////////////////////////
'//大文字小文字の区別ができなかった
    'If Right(ThisWorkbook.Worksheets(2).rngkoban, 1) = "c" Then

    If Right(rngKoban, 1) = "c" Or _
        Right(rngKoban, 1) = "C" Then
'//20170712154419 furukawa ed ////////////////////////

        IsSika = True
    Else
        IsSika = False
    End If
    
End Function


Private Sub Copyworksheets()
On Error GoTo err

    Dim arrsheet As Variant
    arrsheet = Array(ThisWorkbook.Worksheets(1).Name, ThisWorkbook.Worksheets(2).Name)
    
    ThisWorkbook.Worksheets(arrsheet).Copy
    
    Set wb = ActiveWorkbook
    With wb.Sheets(2)
    
        .Unprotect password:=Common_Protect_Password
        'ボタン削除
        .Buttons.Delete
        
        
        .Cells.Font.Name = Common_Font_Name
        .Rows(3).RowHeight = Common_Row_Height
        
        '列幅指定
        .Columns(2).ColumnWidth = ReceCol.ReceColWidth2
        .Columns(3).ColumnWidth = ReceCol.ReceColWidth3
        .Columns(4).ColumnWidth = ReceCol.ReceColWidth4
        .Columns(5).ColumnWidth = ReceCol.ReceColWidth5
        .Columns(6).ColumnWidth = ReceCol.ReceColWidth6
        .Columns(7).ColumnWidth = ReceCol.ReceColWidth7
        
        rngSinryoKikanST.NumberFormatLocal = "@"
        rngSinryoKikanED.NumberFormatLocal = "@"
        
        '//20170704163728 furukawa st ////////////////////////
        '//診療期間の入力規則解除。修正時に入力できない
        rngSinryoKikanST.Validation.Delete
        rngSinryoKikanED.Validation.Delete
        '//20170704163728 furukawa ed ////////////////////////

        

    End With
    Exit Sub
err:
    Call errproc("Copyworksheets")
End Sub

Private Function SaveWorkSheet() As Boolean
On Error GoTo err

    '//20170712142013 furukawa st ////////////////////////
    '//ファイル名をハイフン付きに変換

    Dim strpath As String
    Dim filename As String
    
    '枝番有無でファイル名変更
    If rngEdaban.Value <> "" Then
    
        
        filename = StrConv(StrConv(rngKoban.Value & "-" & _
                rngEdaban, vbNarrow), vbUpperCase) & ".xlsx"
    
    Else
        
        filename = StrConv(StrConv(rngKoban.Value, vbNarrow), vbUpperCase) & ".xlsx"
    
    End If
    
    filename = Left(filename, 6) & "-" & Mid(filename, 7, 3) & "-" & Mid(filename, 10)
    strpath = ThisWorkbook.Path & "\" & filename
    '//20170712142013 furukawa ed ////////////////////////

                        
    '//20170601143705 furukawa st ////////////////////////
    '//原本の別紙４は非表示、完成版は表示させる
    Dim cnt As Integer
    For cnt = 1 To wb.Worksheets.Count
        wb.Sheets(cnt).Visible = True
    Next
    '//20170601143705 furukawa ed ////////////////////////

    On Error Resume Next
    
    wb.SaveAs filename:=strpath
    If err.Number <> 0 Then
        'wb.Close False
        Exit Function
    End If
    
    
    Exit Function
err:
    Call errproc("SaveWorkSheet")
    
    '//20170714091546 furukawa st ////////////////////////
    '//DisplayAlartを消してみる（excel落ちる対策）
    'Application.DisplayAlerts = True
    '//20170714091546 furukawa ed ////////////////////////
    
End Function

Public Sub Initialize()
On Error GoTo err

    
    Set wb = ActiveWorkbook
    
    With wb.Worksheets(2)
        '範囲取得
        Call modRange_RngSika(wb.Worksheets(2))
        
        '個人情報を流用する場合
        If MsgBox("個人情報を削除しますか？", _
            vbQuestion + vbYesNo + vbDefaultButton2) = vbYes Then
                
            'セルの初期化
            rngManage.Value = ""
            rngKojin.Value = ""
        
        End If
        
        If MsgBox("入力したデータを全て削除しますか？", _
            vbQuestion + vbYesNo + vbDefaultButton2) = vbNo Then Exit Sub
        
        
        needpage = 5
        Erase arrSinryoSikibetu
        Erase arrDrug
        
        rngPage1.ClearContents
        rngPage2.ClearContents
        rngPage3.ClearContents
        rngPage4.ClearContents
        rngPage5.ClearContents
        
        '//20170706120123 furukawa st ////////////////////////
        '//診療年月、傷病名、実日数は削除（歯科以外）
        If Not IsSika Then
            rngSinryoYM = ""
            rngSyobyo = ""
            rngNissu = ""
            
        End If
        '//20170706120123 furukawa ed ////////////////////////
        
        
        
        rngBiko = "【備考】"
        
        Dim st As String
        st = ActiveSheet.Name & "!" & cellsSinryoSikibetuHyo_Sika
               
        '入力規則は保護解除が必要
        .Unprotect password:=Common_Protect_Password
        rngSinryoSikibetuCol.Validation.Delete
        rngSinryoSikibetuCol.Validation.Add xlValidateList, , xlEqual, "=" & st
        .Protect password:=Common_Protect_Password, contents:=True, AllowFormattingCells:=True
        
        rngEdaban.Interior.ColorIndex = 0
        rngSakuseisya.Interior.ColorIndex = 0
        rngKoban.Interior.ColorIndex = 0
        rngSinryoKikanST.Interior.ColorIndex = 0
    End With
    
    '//20170602140945 furukawa st ////////////////////////
    '//点数は関数で行うのでセル内削除しない
'    With wb.Worksheets(1)
'        .Unprotect password:=common_protect_password
'         '点数も削除
'        .Range("f49") = 0
'        .Protect password:=common_protect_password, contents:=True, AllowFormattingCells:=True
'    End With
    '//20170602140945 furukawa ed ////////////////////////
    
    Exit Sub
err:
    Call errproc("Initialize")
End Sub



Private Sub GetSinryoSikibetu()
On Error GoTo err

    Dim strSinryoSikibetu As Variant
    Dim r As Integer
    
    '入力されている診療識別を取得
    For Each strSinryoSikibetu In rngSinryoSikibetuCol
        If strSinryoSikibetu <> "" Then
            ReDim Preserve arrSinryoSikibetu(r)
            
            arrSinryoSikibetu(r) = strSinryoSikibetu
            r = r + 1
        End If
    Next
    
    
    Exit Sub
err:
    Call errproc("GetSinryoSikibetu")

End Sub

Private Sub GetDrug()
On Error GoTo err

    
    Dim r As Integer
    Dim rng As Range
    
    
    If IsSika Then
        '歯科の場合
        Set rng = rngSinryoSikibetuCol.Find("(21) 投薬・注射")
    Else
        '医科の場合
        Set rng = rngSinryoSikibetuCol.Find("(20) 投薬")
    End If
    
    If rng Is Nothing Then Exit Sub
    
    Dim rng2 As Range
    Set rng2 = rng.End(xlDown)
    
    
    '入力されている医薬品名を取得
    Dim cnt As Integer
    For r = rng.Row To rng2.Row - 1
        If rng.Offset(cnt, 1) <> "" Then
            ReDim Preserve arrDrug(cnt)
            arrDrug(cnt) = rng.Offset(cnt, 1)
            cnt = cnt + 1
        End If
    Next
    

    Exit Sub
err:
    Call errproc("GetDrug")

End Sub


Private Function ChkUsedRange() As Range
On Error GoTo err
    With wb.Worksheets(2)
        Dim rng As Range
        
        'ページ内に値があればそのページは使用する
        For Each rng In rngPage1
            If rng.Value <> "" Then needpage = 1: Exit For
        Next
        For Each rng In rngPage2
            If rng.Value <> "" Then needpage = 2: Exit For
        Next
        For Each rng In rngPage3
            If rng.Value <> "" Then needpage = 3: Exit For
        Next
        For Each rng In rngPage4
            If rng.Value <> "" Then needpage = 4: Exit For
        Next
        For Each rng In rngPage5
            If rng.Value <> "" Then needpage = 5: Exit For
        Next
        
        '不要ページの削除
        Select Case needpage
            Case 1: .Rows(page1 + 1 & ":" & page5).Delete
            Case 2: .Rows(page2 + 1 & ":" & page5).Delete
            Case 3: .Rows(page3 + 1 & ":" & page5).Delete
            Case 4: .Rows(page4 + 1 & ":" & page5).Delete
        End Select
                
        '//20180510095256 furukawa st ////////////////////////
        '//バージョン表記
        
        '//20180621143155 furukawa st ////////////////////////
        '//バージョン1.055
        '.Range("J60") = "Ver.1054"
        '.Range("J60") = "Ver.1055"
        '//20180621143155 furukawa ed ////////////////////////
        
        '//20180510095256 furukawa ed ////////////////////////

        
        
        
        '保護再開
        .Protect password:=Common_Protect_Password, contents:=True, AllowFormattingCells:=True

    End With
            
    Exit Function
err:
    Call errproc("ChkUsedRange")


End Function

'//20170607133958 furukawa st ////////////////////////
'備考作成
Private Function CreateBiko() As String
    Dim res As String
    Dim tmprange As Range
    For Each tmprange In rngBiko
        If tmprange.Value <> "" Then
            res = res & Replace(tmprange.Value, "【備考】", "") & vbCrLf
        End If
    Next
    
    CreateBiko = res
    
End Function
'診療科区別
Private Function SinryoKaHantei(strVal As String) As String
On Error GoTo err
    Dim res As String
    
    Select Case strVal
        Case "A": res = "医科入院"
        Case "B": res = "医科外来"
        Case "C": res = "歯科"
        Case Else: res = ""
    End Select
    
    SinryoKaHantei = res
    
    Exit Function
err:
    Call errproc("SinryoKaHantei")

End Function

'別紙４作成
Private Sub Create_Bessi4()
On Error GoTo err

    Call modRange_RngBessi4(wb.Worksheets(1))
    
    With wb.Worksheets(1)
        .Unprotect password:=Common_Protect_Password


        '項番
        rngKoban_Bessi4 = StrConv(rngKoban, vbNarrow)
        
        '枝番
        If rngEdaban_Bessi4 <> "" Then rngEdaban_Bessi4 = StrConv(rngEdaban, vbNarrow)
      
        'ページ数
        rngSanteiKanou_Bessi4 = "算定可能"
    
        '//当レセプト作成枚数をページ数にする
        rngPageCount_Bessi4 = needpage
        
        '区別
        rngSinseiKubun_Bessi4 = SinryoKaHantei(Right(StrConv(rngKoban, vbNarrow), 1))
       
            
        '診療期間
        Dim strst As String
        Dim stred As String
        strst = rngSinryoKikanST
        stred = rngSinryoKikanED
        
        '分類
        Dim strSinryoSikibetu As String
        Dim r As Integer
        Dim flg As Boolean
        
        On Error Resume Next
        For r = 0 To UBound(arrSinryoSikibetu)
            strSinryoSikibetu = strSinryoSikibetu & arrSinryoSikibetu(r) & vbCrLf
        Next
        rngSinryoKoui_Bessi4 = strSinryoSikibetu
    
        
        '医薬品
        Dim strdrug As String
        
        For r = 0 To UBound(arrDrug)
            strdrug = strdrug & arrDrug(r) & "、"
        Next
        strdrug = Left(strdrug, Len(strdrug) - 1)
        rngIyakuhin_Bessi4 = strdrug
    
    
        '//20170609163015 furukawa st ////////////////////////
        '//医科歯科とも備考作成
        '備考作成
        rngTokki_Bessi4 = CreateBiko
        '//20170609163015 furukawa ed ////////////////////////
        
        '//20170607144723 furukawa st ////////////////////////
        '//フォントをMSPゴシックに統一（excelバージョンによって勝手に置き換わり印刷範囲がずれる）
        
        '//20180621115028 furukawa st ////////////////////////
        '//フォントをMSゴシックにする（見やすく）
        '.Cells.Font.Name = "ｍｓ ｐゴシック"
        .Cells.Font.Name = "ｍｓ ゴシック"
        '//20180621115028 furukawa ed ////////////////////////
        
        '//20170607144723 furukawa ed ////////////////////////
        
        On Error GoTo err
        
        '//20170706093519 furukawa st ////////////////////////
        '//別紙4は保護しない（山内）
        '.Protect password:=common_protect_password, contents:=True, AllowFormattingCells:=True
        '//20170706093519 furukawa ed ////////////////////////
        
    End With
    
    Exit Sub
err:
    Call errproc("Create_Bessi4")


End Sub

Private Sub CellFormatChange()
On Error GoTo err

    With wb.Worksheets(2)
        '項番
        rngKoban.Value = StrConv(rngKoban.Value, vbNarrow + vbUpperCase)
        
        
         
        If Not IsSika Then rngSinryoYM.Value = clsChk.ChangeDateFormat(rngSinryoYM.Value) '診療年月
        '//20170706121515 furukawa ed ////////////////////////
        
        
        
        rngSinryoKikanED.Value = clsChk.ChangeDateFormat(rngSinryoKikanED.Value) '診療期間終了
        rngSinryoKikanST.Value = clsChk.ChangeDateFormat(rngSinryoKikanST.Value) '診療期間開始
    
        If Not IsSika Then rngBirthDay.Value = clsChk.ChangeDateFormat(rngBirthDay.Value) '生年月日
        
        '診療科判定
        rngTitle = "海外療養費保険適用計算書/" & SinryoKaHantei(StrConv(Right(rngKoban.Value, 1), vbNarrow + vbUpperCase))
        
        '//20170607144906 furukawa st ////////////////////////
        '//改行の削除（印刷範囲がずれる）
        Dim rng As Range
        For Each rng In rngKomoku
            If InStr(rng.Value, vbCrLf) > 0 Then rng = Replace(rng.Value, vbCrLf, "")
        Next
        '//20170607144906 furukawa ed ////////////////////////

        '//20170607144833 furukawa st ////////////////////////
        '//フォントをMSPゴシックに統一（excelバージョンによって勝手に置き換わり印刷範囲がずれる）
        'フォントを固定
        .Cells.Font.Name = "ＭＳ Ｐゴシック"
        '//20170607144833 furukawa ed ////////////////////////
    
    
        '//20170607151130 furukawa st ////////////////////////
        '//履歴削除
        
        '//20170612152652 furukawa st ////////////////////////
        '//履歴削除範囲変更

        .Columns("z:z").Delete
        .Columns("y:y").Delete
        .Columns("x:x").Delete
        
        '//20170612152652 furukawa ed ////////////////////////
        
        '//20170607151130 furukawa ed ////////////////////////
        
        
        '//20170612152934 furukawa st ////////////////////////
        '//西暦和暦換算表削除
        '西暦和暦換算表
        .Range("L12:U39").Delete
        '//20170612152934 furukawa ed ////////////////////////
        
        
                
      
        
    End With
    
    Exit Sub
err:
    Call errproc("CellFormatChange")

End Sub


'
''//20170612160053 furukawa st ////////////////////////
''//日付フォーマット変換処理
'Public Function clsChk.ChangeDateFormat(strVal As String) As String
'    Dim res As String
'    Const HEISEI As Integer = 1988
'    Const SHOWA As Integer = 1925
'
'    If strVal = "" Then Exit Function
'
'
'    Dim strWareki As String
'    Select Case Left(strVal, 1)
'        Case "1": strWareki = "明治"
'        Case "2": strWareki = "大正"
'        Case "3": strWareki = "昭和"
'        Case "4": strWareki = "平成"
'
'    End Select
'
'    Dim y As Integer
'    y = 0
'
'    Select Case Len(strVal)
'        Case 4
'            'eemm 2810
'            '年月のみ
'            Select Case CInt(Left(strVal, 2))
'                 '昭和
'                 Case Is > Format(Now, "ee") + 1
'                     y = Left(strVal, 2) + SHOWA
'
'                 '平成
'                 Case Is <= Format(Now, "ee")
'                     y = Left(strVal, 2) + HEISEI
'
'             End Select
'
'             res = y & "/" & Right(strVal, 2) & "/01"
'             res = Format(res, "gggee年mm月")
'
'        Case 6:
'            'yyyymm 201612
'            If Left(strVal, 2) = "20" Or Left(strVal, 2) = "19" Then
'                res = strVal & "01"
'                res = Left(strVal, 4) & "/" & Mid(strVal, 5, 2) & "/" & Right(strVal, 2)
'                res = Format(res, "ggge年mm月")
'
'            Else
'            'ggmmdd 281010
'                Select Case CInt(Left(strVal, 2))
'                    '昭和
'                    Case Is > Format(Now, "ee") + 1
'                        y = Left(strVal, 2) + SHOWA
'
'                    '平成
'                    Case Is <= Format(Now, "ee")
'                        y = Left(strVal, 2) + HEISEI
'
'                End Select
'                res = y & "/" & Mid(strVal, 3, 2) & "/" & Right(strVal, 2)
'                res = Format(res, "ggge年mm月dd日")
'
'            End If
'
'        Case 8:
'            'yyyymmdd 20161223
'            res = Left(strVal, 4) & "/" & Mid(strVal, 5, 2) & "/" & Right(strVal, 2)
'            res = Format(res, "ggge年mm月dd日")
'
'        Case 7
'            '4281011
'            '日まで
'            res = strWareki & Mid(strVal, 2, 2) & "年" & Mid(strVal, 4, 2) & "月" & Right(strVal, 2) & "日"
'
'        Case 5
'            '42810
'            '年月のみ
'            res = strWareki & Mid(strVal, 2, 2) & "年" & Mid(strVal, 4, 2) & "月"
'
'        '//20170714174502 furukawa st ////////////////////////
'        '//桁数が想定外の場合はそのまま出す
'        Case Else
'            res = strVal
'        '//20170714174502 furukawa ed ////////////////////////
'    End Select
'
'    clsChk.ChangeDateFormat = res
'
'
'End Function
