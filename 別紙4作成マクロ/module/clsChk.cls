VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsChk"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


Public Function ChkMain(ByRef ws As Worksheet) As Boolean
    'エラーカウンタ
    Dim cnterr As Long

    Dim cnterrDate As Long


    '必須項目チェック
    cnterr = cnterr + ChkNecessary(ws)

'//20181119135256 furukawa st ////////////////////////
'//日付エラーと入力エラーを分けた（２重カウントのため）

    '日付関連チェック
    cnterrDate = cnterrDate + ChkDate(ws)
    'cnterr = cnterr + ChkDate(ws)
'//20181119135256 furukawa ed ////////////////////////



    'ほみそ入力チェック
    Dim cntHomiso As Integer
    cntHomiso = ChkHOMISO(ws)
    If cntHomiso > 0 Then
        If MsgBox("[保み想]が点数のないの箇所にありますが宜しいですか？", _
                vbQuestion + vbYesNo + vbDefaultButton2) = vbYes Then
            MsgBox "[保み想]エラーをすべて解除します", vbInformation
            
            Call RemoveHomisoError(ws)
            
        Else
            cnterr = cnterr + cntHomiso
        End If
    End If
    
    
    '診療識別
    If Not ChkSinryoSikibetu(ws) Then
        MsgBox "入力間違いです", vbExclamation
        Exit Function
    End If
    
    
    'エラー数が０超の場合エラー
    If cnterr > 0 Then
        
        
'//20181119135256 furukawa st ////////////////////////
'//日付エラーと入力エラーを分けた（２重カウントのため）

        MsgBox "入力間違い " & cnterr & " 箇所" & vbCrLf & _
                "日付関連間違い " & cnterrDate & " 箇所", vbExclamation
        'MsgBox "入力間違い " & cnterr & " 箇所", vbExclamation
'//20181119135256 furukawa ed ////////////////////////


        Exit Function
    Else
        ChkMain = True
    End If
        
End Function

'チェックで入力ミスがあるセルは背景赤
Private Sub ErrCell(ByRef cnterr As Long, ByRef rng As Range)
    rng.Interior.Color = RGB(250, 0, 0)
    rng.Font.Color = RGB(255, 255, 255)
    cnterr = cnterr + 1
End Sub

'チェックで入力ミスがないセルは背景透明
Private Sub NoneErrCell(ByRef rng As Range)
    rng.Interior.ColorIndex = xlNone
    rng.Font.Color = RGB(0, 0, 0)
End Sub

'診療識別入力チェック
Public Function ChkSinryoSikibetu(ws As Worksheet) As Boolean
    
    Dim rngChk As Range
    Set rngChk = rngSinryoSikibetuCol
    Dim c As Range
    
    Dim cntInput As Long
    cntInput = 0
    
    '範囲内に一つでも何か入っていたらOK
    For Each c In rngChk.Cells
        If c.Value <> "" Then
            cntInput = cntInput + 1
            Exit For
        End If
    Next
    
    If cntInput = 0 Then
        '何もない場合赤にする
        rngChk.Interior.Color = RGB(255, 0, 0)
        ChkSinryoSikibetu = False
    Else
        '一つでもあればOKにする
        rngChk.Interior.ColorIndex = xlNone
        ChkSinryoSikibetu = True
    End If


End Function

'診療識別列エラー解除
Public Sub RemoveSinryoSikibetuErr(ws As Worksheet)
    Dim rngChk As Range
    Set rngChk = rngSinryoSikibetuCol


    rngChk.Interior.ColorIndex = xlNone
    

End Sub


'ほみそチェック
Public Function ChkHOMISO(ws As Worksheet) As Integer
    
    Dim rngChk As Range
    Set rngChk = rngKomoku
    Dim c As Range
    Dim cnterr As Long
    cnterr = 0
    
    For Each c In rngChk.Cells
    
        '点数、数量とも入っている
        If c.Offset(0, 1).Value <> "" And c.Offset(0, 2).Value <> "" Then
            
            '入っていないと背景を赤くする
            If c.Offset(0, 4) = "" Then
                Call ErrCell(cnterr, c.Offset(0, 4))
            Else
                Call NoneErrCell(c.Offset(0, 4))
            End If
            
        '点数、数量とも入ってない
        Else
            '入ってると背景を赤くする
            If c.Offset(0, 4) <> "" Then
                Call ErrCell(cnterr, c.Offset(0, 4))
            Else
                Call NoneErrCell(c.Offset(0, 4))
            End If
                        
        End If
    Next
    
    'エラー数を取得
    ChkHOMISO = cnterr
    
End Function
'ほみそエラーの解除
Public Sub RemoveHomisoError(ws As Worksheet)
    Dim rngChk As Range
    Set rngChk = rngKomoku
    Dim c As Range
  
    For Each c In rngChk.Cells
        Call NoneErrCell(c.Offset(0, 4))
    Next
    
End Sub

'必須項目入力チェック
Public Function ChkNecessary(ByRef ws As Worksheet) As Integer

    '個人情報＋管理情報、傷病名、実日数
    Dim rngNecessary As Range
    Dim cellsNecessary As String
    cellsNecessary = cellsKojin & "," & cellsManage
    Set rngNecessary = ws.Range(cellsNecessary)
    
    
    Dim c As Range
    Dim cnterr As Long
    
    For Each c In rngNecessary.Cells
        '空の場合エラーとする
        If c.Value = "" Then
            c.Interior.Color = RGB(255, 0, 0)
            cnterr = cnterr + 1
        Else
            c.Interior.Color = xlNone
        End If
    Next
    
    'エラー数を取得
    ChkNecessary = cnterr
       

End Function


'必須入力書式チェック
Private Function ChkFormatNecessary() As Boolean

    Set rngKoban = Range(cellsEdaban): rngKoban.Interior.ColorIndex = 0                           '項番
    Set rngSinryoKikanST = Range(cellsIryokikan): rngSinryoKikanST.Interior.ColorIndex = 0        '診療機関名
    Set rngSakuseisya = Range(cellsSakuseisya): rngSakuseisya.Interior.ColorIndex = 0             '作成者名
    Set rngSinryoYM = Range(cellsSinryoKikanST): rngSinryoYM.Interior.ColorIndex = 0              '診療年月開始
    Set rngSinryoKikanED = Range(cellsSinryoKikanED): rngSinryoKikanED.Interior.ColorIndex = 0    '診療年月終了
    Set rngSyobyo = Range(cellsSyobyo): rngSyobyo.Interior.ColorIndex = 0                         '傷病名
    Set rngNissu = Range(cellsNissu): rngNissu.Interior.ColorIndex = 0                            '実日数
    Set rngEdaban = Range(cellsEdaban): rngEdaban.Interior.ColorIndex = 0                         '枝番
    Set rngBirthDay = Range(cellsBirthday): rngBirthDay.Interior.ColorIndex = 0                   '生年月日
    
    
    '項番桁数
    If Len(rngKoban) < 11 Then
        rngKoban.Interior.Color = RGB(255, 0, 0)
        MsgBox "項番の桁数を確認してください", vbExclamation
        ChkFormatNecessary = False
        Exit Function
    End If
    
    '項番あたま2桁（神奈川支部）
    If Left(rngKoban, 2) <> "14" Then
        rngKoban.Interior.Color = RGB(0, 200, 200)
        If MsgBox("項番の左二桁が14ではありませんが宜しいですか？", _
            vbQuestion + vbYesNo + vbDefaultButton2) = vbNo Then
            ChkFormatNecessary = False
            Exit Function
        End If
    End If
    
    
    '半角英大数に
    rngKoban.Value = StrConv(rngKoban.Value, vbNarrow + vbUpperCase)


End Function

Public Function ChkDate(ByRef ws As Worksheet) As Integer
    Dim cnterr As Long
    
'//20190404135400 furukawa st ////////////////////////
'//日付チェック関数を作成

    If Not IsDateEx(rngSinryoKikanST) Then
        Call ErrCell(cnterr, rngSinryoKikanST)
    Else
        Call NoneErrCell(rngSinryoKikanST)
    End If
    
    If Not IsDateEx(rngSinryoKikanED) Then
        Call ErrCell(cnterr, rngSinryoKikanED)
    Else
        Call NoneErrCell(rngSinryoKikanED)
    End If
    
    
    If Not IsDateEx(rngSinryoYM) Then
        Call ErrCell(cnterr, rngSinryoYM)
    Else
        Call NoneErrCell(rngSinryoYM)
    End If
    
    If Not IsDateEx(rngBirthDay) Then
        Call ErrCell(cnterr, rngBirthDay)
    Else
        Call NoneErrCell(rngBirthDay)
    End If
        
        
'    If Not IsDate(rngSinryoKikanST) Then
'        Call ErrCell(cnterr, rngSinryoKikanST)
'    Else
'        Call NoneErrCell(rngSinryoKikanST)
'    End If
'
'    If Not IsDate(rngSinryoKikanED) Then
'        Call ErrCell(cnterr, rngSinryoKikanED)
'    Else
'        Call NoneErrCell(rngSinryoKikanED)
'    End If
'
'
'    If Not IsDate(rngSinryoYM) Then
'        Call ErrCell(cnterr, rngSinryoYM)
'    Else
'        Call NoneErrCell(rngSinryoYM)
'    End If
'
'    If Not IsDate(rngBirthDay) Then
'        Call ErrCell(cnterr, rngBirthDay)
'    Else
'        Call NoneErrCell(rngBirthDay)
'    End If
    
    
'//20190404135400 furukawa ed ////////////////////////
    
    ChkDate = cnterr
    
End Function


'//20190404135516 furukawa st ////////////////////////
'//日付チェック関数令和対策
'//20190404135516 furukawa ed ////////////////////////
Private Function IsDateEx(rng As Range) As Boolean
    If Not IsDate(rng) Then
        If InStr(1, rng, "令和", vbTextCompare) > 0 Then
            IsDateEx = True
            Exit Function
        Else
            IsDateEx = False
            Exit Function
        End If
    Else
        IsDateEx = True
        Exit Function
    End If
    
End Function

Public Function ChangeDate() As String
    
    '日付書式
    rngSinryoKikanST.Value = ChangeDateFormat(rngSinryoKikanST.Value) '診療期間終了
    rngSinryoKikanED.Value = ChangeDateFormat(rngSinryoKikanED.Value) '診療期間開始
    
    rngSinryoYM.Value = ChangeDateFormat(rngSinryoYM.Value) '診療年月
    rngBirthDay.Value = ChangeDateFormat(rngBirthDay.Value) '生年月日
        
End Function


'日付書式変換
Public Function ChangeDateFormat(strVal As String) As String
    Dim res As String
    
    
    '//20190404112311 furukawa st ////////////////////////
    '//令和追加
    Const REIWA As Integer = 2018
    '//20190404112311 furukawa ed ////////////////////////
    
    Const HEISEI As Integer = 1988
    Const SHOWA As Integer = 1925
    
    If strVal = "" Then Exit Function


    If IsDate(strVal) Then
        ChangeDateFormat = strVal
        Exit Function
    End If

    Dim strWareki As String
    Select Case Left(strVal, 1)
        Case "1": strWareki = "明治"
        Case "2": strWareki = "大正"
        Case "3": strWareki = "昭和"
        Case "4": strWareki = "平成"
                    
        '2019-04-03furukawa add
        Case "5": strWareki = "令和"
        
    End Select



    Dim strY As String
    Dim strM As String
    Dim strD As String
    
    Dim dttmp As Date


    Select Case Len(strVal)
        Case 4
            
            Dim y As Integer
            y = 0
                    
            '//20190404101947 furukawa st ////////////////////////
            '//４桁では判別不能　０１０１平成？令和？

            ChangeDateFormat = False
            Exit Function
'
'            'eemm 2810
'            '年月のみ
'            Select Case CInt(Left(strVal, 2))
'                 '昭和 今年未満の場合、昭和とみなす
'                 Case Is > Format(Now, "ee") + 1
'                     y = Left(strVal, 2) + SHOWA
'
'                 '平成
'                 Case Is <= Format(Now, "ee")
'
'                    y = Left(strVal, 2) + HEISEI
'
'             End Select
'
'             res = y & "/" & Right(strVal, 2) & "/01"
'             res = Format(res, "gggee年mm月")

        
'//20190404101947 furukawa ed ////////////////////////
        
        Case 6:
            '西暦　年月yyyymm 201612
            If Left(strVal, 2) = "20" Or Left(strVal, 2) = "19" Then
                res = strVal & "01"
                res = Left(strVal, 4) & "/" & Mid(strVal, 5, 2) & "/" & Right(strVal, 2)
                
                '//20190404102326 furukawa st ////////////////////////
                '//yyyymmの場合、２０１９年５月は令和とする
                
                dttmp = CDate(res)
                If Year(dttmp) >= 2019 And Month(dttmp) >= 5 Then
                    res = "令和" & Year(dttmp) - REIWA & "年" & Month(dttmp) & "月"
                Else
                    res = Format(res, "ggge年mm月")
                End If
                
                'res = Format(res, "ggge年mm月")
                
                '//20190404102326 furukawa ed ////////////////////////
                
            Else
                
                '//20190404102552 furukawa st ////////////////////////
                '//４桁では判別不能　０１０１平成？令和？

                ChangeDateFormat = False
                Exit Function
                
                
'            'ggmmdd 281010
'                Select Case CInt(Left(strVal, 2))
'                    '昭和
'                    Case Is > Format(Now, "ee") + 1
'                        y = Left(strVal, 2) + SHOWA
'
'                    '平成
'                    Case Is <= Format(Now, "ee")
'                        y = Left(strVal, 2) + HEISEI
'
'                End Select
'                res = y & "/" & Mid(strVal, 3, 2) & "/" & Right(strVal, 2)
'                res = Format(res, "ggge年mm月dd日")
                
                
                '//20190404102552 furukawa ed ////////////////////////
                
            End If
            
        Case 8:
        
            '西暦　年月日　yyyymmdd 20161223
            
            '//20190404103500 furukawa st ////////////////////////
            '//yyyymmの場合、２０１９年５月は令和とする

            res = Left(strVal, 4) & "/" & Mid(strVal, 5, 2) & "/" & Right(strVal, 2)
            
            dttmp = CDate(res)
            
            '//20200508131412 furukawa st ////////////////////////
            '//日付チェック関数2020年以降令和対策

            If (Year(dttmp) >= 2019 And Month(dttmp) >= 5) Or (Year(dttmp) >= 2020) Then
            'If Year(dttmp) >= 2019 And Month(dttmp) >= 5 Then
                
            '//20200508131412 furukawa ed ////////////////////////
            
            
                res = "令和" & Format(Year(dttmp) - REIWA, "00") & "年" & _
                            Format(Month(dttmp), "00") & "月" & _
                            Format(Day(dttmp), "00") & "日"
                
            Else
                res = Format(res, "ggge年mm月dd日")
            End If
            
            'res = Format(res, "ggge年mm月dd日")
            
            '//20190404103500 furukawa ed ////////////////////////


        Case 7, 5
            '和暦番号＋和暦年月日
            '4281011
            '日まで
            
            '和暦番号＋年月
            '42810
            '年月のみ
            
            
            '//20190404115830 furukawa st ////////////////////////
            '//和暦返し

            '//20190410200430 furukawa st ////////////////////////
            '//50105対応

            Select Case Len(strVal)
            Case 7
                strY = Format(Mid(strVal, 2, 2), "00")
                strM = Format(Mid(strVal, 4, 2), "00")
                strD = Format(Right(strVal, 2), "00")
            Case 5
                strY = Format(Mid(strVal, 2, 2), "00")
                strM = Format(Mid(strVal, 4, 2), "00")
                strD = "01"
            End Select
'
'            strY = Format(Mid(strVal, 2, 2), "00")
'            strM = Format(Mid(strVal, 4, 2), "00")
'
'            If Len(strVal) = 7 Then
'                strD = Format(Right(strVal, 2), "00")
'            Else
'                strD = "01"
'            End If
            '//20190410200430 furukawa ed ////////////////////////

            
            If strWareki = "平成" And CInt(strY) >= 31 And CInt(strM) >= 5 Then ' 平成31年５月以降の場合令和に直す
                If Len(strVal) = 7 Then res = "令和" & Format(CInt(strY) - 30, "00") & "年" & strM & "月" & strD & "日"
                If Len(strVal) = 5 Then res = "令和" & Format(CInt(strY) - 30, "00") & "年" & strM & "月"
            
            ElseIf strWareki = "令和" And CInt(strY) = 1 And CInt(strM) <= 4 Then '令和１年４月以前の場合平成に直す
                If Len(strVal) = 7 Then res = "平成" & Format(CInt(strY) + 30, "00") & "年" & strM & "月" & strD & "日"
                If Len(strVal) = 5 Then res = "平成" & Format(CInt(strY) + 30, "00") & "年" & strM & "月"
            
            ElseIf strWareki = "令和" And CInt(strY) > 1 Then '令和２年以降の場合令和でいく
                If Len(strVal) = 7 Then res = "令和" & Format(CInt(strY) + 30, "00") & "年" & strM & "月" & strD & "日"
                If Len(strVal) = 5 Then res = "令和" & Format(CInt(strY) + 30, "00") & "年" & strM & "月"
            
            
            '//20190410200518 furukawa st ////////////////////////
            '//50105対応

            ElseIf strWareki = "令和" And CInt(strY) = 1 And CInt(strM) >= 5 Then '令和１年５月以上の場合
                If Len(strVal) = 7 Then res = "令和" & strY & "年" & strM & "月" & strD & "日"
                If Len(strVal) = 5 Then res = "令和" & strY & "年" & strM & "月"
            
            '//20190410200518 furukawa ed ////////////////////////


            Else
            
                Dim diffYear As Long
                
                Select Case strWareki
                    Case "昭和"
                        diffYear = 1925
                    Case "平成"
                        diffYear = 1988
                    Case "大正"
                        diffYear = 1911
                    Case "明治"
                        diffYear = 1867
                                                
                End Select
                
                
                dttmp = CDate(CInt(strY) + diffYear & "/" & strM & "/" & strD)
                
                If Len(strVal) = 7 Then res = Format(dttmp, "ggge年mm月dd日")
                If Len(strVal) = 5 Then res = Format(dttmp, "ggge年mm月")
        
        
            
            End If
            
            
            'res = strWareki & Mid(strVal, 2, 2) & "年" & Mid(strVal, 4, 2) & "月" & Right(strVal, 2) & "日"
            
            
            '//20190404115830 furukawa ed ////////////////////////
        
        
        
        
        
        
        '//20190404115831 furukawa st ////////////////////////
        '//まとめたので不要

        'Case 5
            
            '和暦番号＋年月
            '42810
            '年月のみ
                        
            
            'res = strWareki & Mid(strVal, 2, 2) & "年" & Mid(strVal, 4, 2) & "月"
        
        
        '//20190404115831 furukawa ed ////////////////////////
                        
                        
                        
                        
                        
        '//20170714174502 furukawa st ////////////////////////
        '//桁数が想定外の場合はそのまま出す
        Case Else
            res = strVal
        '//20170714174502 furukawa ed ////////////////////////
    End Select
    
        
    
    If Not IsDate(res) Then
        If InStr(1, res, "令和", vbTextCompare) >= 1 Then
            ChangeDateFormat = res
        Else
            ChangeDateFormat = "エラー"
            Exit Function
        End If
    End If
    
    ChangeDateFormat = res
        

End Function
