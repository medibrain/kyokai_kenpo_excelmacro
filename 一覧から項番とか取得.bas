Attribute VB_Name = "一覧から項番とか取得"
Option Explicit
Dim arrKoban() As String
Dim last As Long
Dim fso As Object

Sub main()
    getinfo
    addbook
    
 
End Sub
        
Sub getinfo()
    With ThisWorkbook.Worksheets(1)
        Dim rng As Range
        
        Set rng = .Cells.Find("項番")
        last = rng.End(xlDown).Row
        last = last - rng.Row + 1
        Dim r As Integer
        For r = 1 To last
            ReDim Preserve arrKoban(3, r - 1)
            arrKoban(0, r - 1) = rng.Offset(r, 0)
            arrKoban(1, r - 1) = rng.Offset(r, 1)
            arrKoban(2, r - 1) = rng.Offset(r, 2)
            arrKoban(3, r - 1) = rng.Offset(r, 5)
        Next
        
    End With
End Sub

Sub addbook()
    Dim strpath As String
    strpath = ThisWorkbook.Path & "\excel"
    Dim wbs As Workbooks
    
    Dim wb  As Workbook
    Dim ws As Worksheet
    
    
    Dim wbbessi As Workbook
    Dim wsbessi As Worksheet
    
    Workbooks.Open strpath & "\【別紙４】結果回答書.xlsx"
    Set wbbessi = Workbooks("【別紙４】結果回答書.xlsx")
    Set wsbessi = wbbessi.Worksheets(1)
    
    Dim file
    Set fso = CreateObject("scripting.filesystemobject")
    Dim r As Integer
    
    For r = 0 To last
            Dim samecnt
            samecnt = 0
            For Each file In fso.getfolder(strpath).Files
                If file.Name Like arrKoban(0, r) & "*.xlsx" Then
                    samecnt = samecnt + 1
                End If
            Next
        For Each file In fso.getfolder(strpath).Files
            If file.Name Like arrKoban(0, r) & "*.xlsx" Then
        
'        If fso.fileexists(strpath & "\" & arrKoban(0, r) & ".xlsx") Or _
'         fso.fileexists(strpath & "\" & arrKoban(0, r) & "*.xlsx") Then
            Set wb = Workbooks.Open(strpath & "\" & file.Name)
            
            Set wsbessi = wbbessi.Worksheets(1)
            
            If wb.Worksheets(1).Name <> "【別紙４】結果回答書" Then
                wsbessi.Copy wb.Sheets(1)
            
    '            wb.Worksheets.Add wsbessi
                Set wsbessi = wb.Sheets(1)
                Set ws = wb.Worksheets(2)
                
                With wsbessi
                    .Range("s3") = "平成29年　　月　　日"
                    .Range("a5") = "神奈川"
                    .Range("c17") = arrKoban(0, r)
                    If samecnt = 1 Then
                    .Range("f17") = 1
                    Else
                    .Range("f17") = Right(fso.getbasename(wb.FullName), 1)
                    End If
                    
                    
                    .Range("i17") = arrKoban(1, r)
                    .Range("m17") = arrKoban(2, r)
                    .Range("r17") = arrKoban(3, r)
                    
                    .Range("h22") = samecnt
                    .Range("h25") = 1
                    .Range("p25") = "算定可能"
                    
                    .Range("f28") = ws.Range("f6")
                    .Range("m28") = "歯科"
                    Dim ed
                    Dim dt As Date
                    
                    If Month(Format(ws.Range("f6"), "gggee年mm月1日")) + 1 > 12 Then
                    
                        ed = _
                        Year(Format(ws.Range("f6"), "gggee年mm月1日")) + 1 & "/" & _
                         1 & "/" & _
                        Day(Format(ws.Range("f6"), "gggee年mm月1日"))
                    
                    Else
    
                        ed = _
                        Year(Format(ws.Range("f6"), "gggee年mm月1日")) & "/" & _
                        Month(Format(ws.Range("f6"), "gggee年mm月1日")) + 1 & "/" & _
                        Day(Format(ws.Range("f6"), "gggee年mm月1日"))
                    End If
                    
                     
                    
                    dt = CDate(ed) - 1
                    ed = Format(dt, "gggee年m月dd日")
                    .Range("f30") = ws.Range("f6") & "1日" & "〜" & ed 'ws.Range("f6") & "31日"
                    .Range("f32") = ws.Range("c11")
                    .Range("u30") = ws.Range("g12")
                    .Range("f49") = WorksheetFunction.Sum(ws.Range("f15:f51"))
                    .Range("v62") = "大島　かおる"
                    
                End With
            End If
            Application.DisplayAlerts = False
             wb.Save
            Application.DisplayAlerts = True
            wb.Close
            Set wb = Nothing
            Set ws = Nothing
            Set wsbessi = Nothing
            
        End If
     
        Next
        
    Next
    Set fso = Nothing
    
    MsgBox "終了"
End Sub
