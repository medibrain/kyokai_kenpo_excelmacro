Attribute VB_Name = "作業表作成個人わけ"
Const strsheetname = "作業表"

Sub main()
    管理表作成
    個人分け

End Sub

Private Sub 管理表作成()
    
    Dim ws As Worksheet
    
    With ThisWorkbook
        Dim ws1 As Worksheet
        Set ws1 = .Worksheets("別紙２−１（翻訳・レセプト作成業務状況一覧表）")
        
        .SaveCopyAs .Path & "\backup.xlsx"
    
        For Each ws In .Worksheets
            If ws.Name = strsheetname Then
                ws.Delete
                Exit For
            End If
        Next
        
        Set ws = .Worksheets.Add(, ws1)
        ws.Name = strsheetname

        'ヘッダ文字列を指定し、カンマ区切り
        ws.Range("a1") = "行数,項番,セット番号,支部コード,依頼年月,月内通番,委託区分,種別,作成者,エクセル枚数,作成完了日," & _
        "在宅ﾁｪｯｸ,山内ﾁｪｯｸ,Chkエクセル数,Chkワード数,ChkPDF数,Chk合計点数,記号,番号,国名,外国語,翻訳,memo1,提出完了,記号番号"
        
        ws.Range("a1").TextToColumns Cells(Selection.Row, Selection.Column), xlDelimited, xlTextQualifierNone, , , , True
        
        Dim rngcopy As Range
        Dim rngKoban As Range
        Dim lastrow As Long
        
        Set rngKoban = ws1.Cells.Find("項番")
        lastrow = rngKoban.End(xlDown).Row
        
        '委託区分でフィルタ
'        ws1.Range(ws1.Cells(rngKoban.Row, 1), _
'                             ws1.Cells(lastrow, 24)).AutoFilter rngKoban.Column - 2, "2"
                            
        
        'オリジナルシートの必要部分をコピペ
        Set rngcopy = ws1.Range(ws1.Cells(rngKoban.Row + 1, rngKoban.Offset(0, -5).Column), _
                                            ws1.Cells(lastrow, rngKoban.Offset(0, -1).Column))
        rngcopy.Copy
        'ws.Range("d2").PasteSpecial
        ws.Cells(2, ws.Cells.Find("支部コード").Column).PasteSpecial
        
        Set rngcopy = ws1.Range(ws1.Cells(rngKoban.Row + 1, rngKoban.Offset(0, 1).Column), _
                                            ws1.Cells(lastrow, rngKoban.Offset(0, 4).Column))
        rngcopy.Copy
        ws.Cells(2, ws.Cells.Find("記号").Column).PasteSpecial
        
        ws.Cells(ws.Cells.Find("エクセル枚数").Column).NumberFormat = "G/標準"
    End With
    
    With ws
      
        
        Dim c As Range
        Dim r As Long
        Dim cnt As Long
        
        Dim rngKigoBango As Range
        Set rngKigoBango = .Cells.Find("記号番号")
        Dim rngKigo As Range
        Set rngKigo = .Cells.Find("記号", , , , , , True)
        
        
        Set rngKoban = .Cells.Find("項番")
        
        .Columns(rngKigoBango.Column).NumberFormat = "@"
        .Columns(rngKigo.Column).NumberFormat = "@"
        .Columns(rngKigo.Column + 1).NumberFormat = "@"
        
        cnt = 0
        For r = 2 To lastrow - (rngKoban.Row - 1)
            '行番号
            .Cells(r, 1).Formula = "=Row()-1"
            
            '40行で改ページ
            If cnt Mod 40 = 0 And cnt > 1 Then .Cells(r, 1).PageBreak = xlPageBreakManual
            
            
            '項番作成
            .Cells(r, rngKoban.Column).Formula = "=d" & r & _
                                    " & e" & r & _
                                    " & f" & r & _
                                    " & g" & r & _
                                    " & h" & r
                        
            '記号・番号
            
            .Cells(r, rngKigoBango.Column).Value = _
                CStr(.Cells(r, rngKigo.Column).Value & .Cells(r, rngKigo.Column + 1).Value)
            
             cnt = cnt + 1
        Next
        
        '項番を値で貼り付ける
        .Columns(rngKoban.Column).Copy
        .Columns(rngKoban.Column).PasteSpecial xlPasteValues
        
        Call 体裁(ws)

        
    End With
    
End Sub


Private Sub 体裁(ws As Worksheet)
    With ws
        'シートの体裁（山内希望）
        
        'フォント関連
        .Cells.Font.Name = "ＭＳ ゴシック"
        .Cells.Font.Size = 10
        
        .Columns("a:a").Font.Size = 10
        .Columns("b:b").Font.Size = 8
        .Columns("c:c").Font.Size = 12
        .Columns("e:f").Font.Size = 14
        
        
        .Columns.AutoFit
        
        '各行の高さ
        .UsedRange.RowHeight = 20
        
        .Columns(.Cells.Find("国名").Column).ColumnWidth = 10
        
        
        '外国語の広さ
        '.Columns("s:s").ColumnWidth = 10
        .Columns(.Cells.Find("外国語").Column).ColumnWidth = 10
        
        
        'メモの広さ
        '.Columns("u:u").ColumnWidth = 20
        .Columns(.Cells.Find("memo1").Column).ColumnWidth = 20
        
        
        'ヘッダ行
        .Rows("1:1").RowHeight = 35
        .Rows("1:1").WrapText = True
    
        '列の広さ
        '.Columns("c:d").ColumnWidth = 7
        .Columns(.Cells.Find("セット番号").Column).ColumnWidth = 5
        .Columns(.Cells.Find("支部コード").Column).ColumnWidth = 4
        
        
        .Columns("e:f").ColumnWidth = 7
        .Columns("g:h").ColumnWidth = 5
        .Columns("L:p").ColumnWidth = 6
    
        '枠線
        .UsedRange.Borders.LineStyle = xlContinuous
        .UsedRange.Borders.Weight = xlHairline
        
        '印刷設定
        .PageSetup.Zoom = 70
        .PageSetup.Orientation = xlLandscape
        .PageSetup.TopMargin = 20
        .PageSetup.BottomMargin = 30
        .PageSetup.RightMargin = 10
        .PageSetup.LeftMargin = 10
        
        .PageSetup.PrintTitleRows = "$1:$1"
        .PageSetup.CenterFooter = "&P/&N"
        
        .PageSetup.PrintArea = Range("a:y").Address
        .PageSetup.Zoom = 60
        
    End With
End Sub

Private Sub 個人分け()
    Dim dic() As String
    Dim lastcol As Integer
    Dim lastrow As Long
    Dim rng As Range
    
    Dim SetNoCol As Integer 'セット番号の列
    Dim KigoBangoCol As Integer '記号番号の列
    
    With ThisWorkbook.Worksheets(strsheetname)
        
        .Cells.Interior.ColorIndex = xlNone '色初期化
        
        '最後取得
        lastcol = .Range("a1").End(xlToRight).Column
        lastrow = .Range("a1").End(xlDown).Row
        
        
        Set rng = .Cells.Find("セット番号")
        SetNoCol = rng.Column
        
        .Range(.Cells(2, SetNoCol), .Cells(lastrow, SetNoCol)).ClearContents
        .Range(.Cells(2, SetNoCol), .Cells(lastrow, SetNoCol)).NumberFormat = "@"
        
        Set rng = .Cells.Find("記号番号")
        KigoBangoCol = rng.Column
        
        Dim c As Range
        Dim r As Long
        r = 1
        
        'dicに記号番号を入れる
        For Each c In .Range(.Cells(2, KigoBangoCol), .Cells(lastrow, KigoBangoCol))
            
            Dim cnt As Integer
            Dim flg
            
            On Error GoTo 1
            'dicに既に存在する記号番号は入れない
            For cnt = 0 To UBound(dic, 2)
                If dic(0, cnt) = c.Value Then
                    flg = True
                     Exit For
                Else
                    flg = False
                End If
                
            Next
            
1:
            If Not flg Then
                ReDim Preserve dic(1, r)
                dic(0, r) = c.Value
                dic(1, r) = Format(CStr(r), "000")
                r = r + 1
            End If
             
        Next
        
        '最初から回す
        For r = 0 To lastrow
        
            '記号番号と比較する
            Set c = .Cells(r + 1, KigoBangoCol)
            
            For cnt = 1 To UBound(dic, 2)
                
                If dic(0, cnt) = c.Value Then
                    'セット番号入れる
                    .Cells(r + 1, SetNoCol).Value = dic(1, cnt)
                    
                    '色つけ
                    If dic(1, cnt) Mod 2 = 0 Then .Range(.Cells(c.Row, 1), .Cells(c.Row, KigoBangoCol)).Interior.ColorIndex = 15
                    If dic(1, cnt) Mod 2 = 1 Then .Range(.Cells(c.Row, 1), .Cells(c.Row, KigoBangoCol)).Interior.ColorIndex = 0
'                    If dic(1, cnt) Mod 2 = 0 Then .Rows(c.Row).Interior.ColorIndex = 15
'                    If dic(1, cnt) Mod 2 = 1 Then .Rows(c.Row).Interior.ColorIndex = xlNone
                End If
            Next
        Next
        
        '最終ソート
        .Cells.Sort key1:="委託区分", order1:=xlDescending, _
                    key2:="セット番号", order2:=xlAscending, _
                    key3:="月内通番", order3:=xlAscending, _
                    Header:=xlYes
    
    End With
    
    Call toukei
    
    MsgBox "終了"
    
End Sub

Private Sub toukei()
On Error GoTo err

    Dim cn As Object
    Dim rs As Object
    Set cn = CreateObject("adodb.connection")
    Set rs = CreateObject("adodb.recordset")
    cn.connectionstring = "provider=microsoft.ace.oledb.12.0;data source=" & _
                    ThisWorkbook.FullName & ";extended properties=""excel 12.0;hdr=yes"""
    cn.Open
    Dim ws As Worksheet
    For Each ws In ThisWorkbook.Sheets
        If ws.Name = "セット番号フォルダ委託区分" Then
            ws.Delete
            Exit For
        End If
    Next
    
    Set ws = ThisWorkbook.Worksheets.Add
    ws.Name = "セット番号フォルダ委託区分"
    
    
    Dim strsql As String
    
    strsql = ""
    '委託区分＝１のフォルダ
    strsql = "select セット番号 from [作業表$] where 委託区分=1 group by セット番号"
    
    rs.Open strsql, cn
    
    Dim r As Long
    r = 1
    
    With ws
        .Cells(r, 1).Value = "委託区分 = 1"
        
        r = r + 1
        Do While Not rs.EOF
            .Cells(r, 1).Value = rs("セット番号")
            rs.movenext
            r = r + 1
        Loop
    End With
    
    
    rs.Close
    
    
    
    strsql = ""
    '委託区分＝2のフォルダ
    strsql = "select セット番号 from [作業表$] where 委託区分=2 group by セット番号"
    
    rs.Open strsql, cn
    
    
    r = 1
    
    With ws
        .Cells(r, 2).Value = "委託区分 = 2"
        
        r = r + 1
        Do While Not rs.EOF
            .Cells(r, 2).Value = rs("セット番号")
            rs.movenext
            r = r + 1
        Loop
            
    End With
    rs.Close
    
      strsql = ""
    '委託区分＝2のフォルダ
    strsql = "select A.セット番号,B.セット番号 from (select セット番号 from [作業表$] where 委託区分=1) A " & _
     "inner join (select セット番号 from [作業表$] where 委託区分=2) B " & _
     "on A.セット番号=B.セット番号 group by A.セット番号,B.セット番号"
    
    rs.Open strsql, cn
    
    
    r = 1
    
    With ws
        .Cells(r, 3).Value = "委託区分 = 1 and 2"
        
        r = r + 1
        Do While Not rs.EOF
            .Cells(r, 3).Value = rs("A.セット番号")
            rs.movenext
            r = r + 1
        Loop
            
    End With
    rs.Close
    
    
    
    cn.Close
    
    ws.Columns.AutoFit
    
    Exit Sub

err:
    MsgBox err.Description
    rs.Close
    cn.Close

    
End Sub
