	if WScript.Arguments.count=0 then 
		msgbox "引数が足りません"
		WScript.Quit(-1)
	end if

	on error resume next
	dim strsheetname
	strsheetname = "作業表"
    
    Dim exapp,twb,ws
    dim strExcelFileName

    strExcelFileName=WScript.Arguments(0)

    set exapp=createobject("excel.application")
    set twb=exapp.workbooks.open(strExcelFileName)
    
    With twb
    
        .SaveCopyAs .Path & "\backup.xlsx"

        For Each ws In .Worksheets
            If ws.Name = strsheetname Then
                ws.delete
                Exit For
            End If
        Next
        
        
        Set ws = .Worksheets.Add(,.Worksheets(1))
        ws.Name = strsheetname

        'ヘッダ文字列を指定し、カンマ区切り
		ws.cells(1,1)="行数"
		ws.cells(1,2)="項番"
		ws.cells(1,3)="セット番号"
		ws.cells(1,4)="支部コード"
		ws.cells(1,5)="依頼年月"
		ws.cells(1,6)="月内通番"
		ws.cells(1,7)="委託区分"
		ws.cells(1,8)="種別"
		ws.cells(1,9)="作成者"
		ws.cells(1,10)="エクセル数"
		ws.cells(1,11)="ワード数"
		ws.cells(1,12)="PDF数"
		ws.cells(1,13)="合計点数"
		ws.cells(1,14)="作成完了日"
		ws.cells(1,15)="在宅ﾁｪｯｸ"
		ws.cells(1,16)="山内ﾁｪｯｸ"
		ws.cells(1,17)="記号"
		ws.cells(1,18)="番号"
		ws.cells(1,19)="国名"
		ws.cells(1,20)="外国語"
		ws.cells(1,21)="翻訳"
		ws.cells(1,22)="memo1"
		ws.cells(1,23)="提出完了"
		ws.cells(1,24)="記号番号"
		       
        
        Dim rngcopy
        Dim rngKoban
        Dim lastrow
        
        Set rngKoban = .Worksheets(1).Cells.Find("項番")
        lastrow = rngKoban.End(-4121).Row
                
        'オリジナルシートの必要部分をコピペ
        Set rngcopy = .Worksheets(1).Range(.Worksheets(1).Cells(rngKoban.Row + 1, rngKoban.Offset(0, -5).Column), _
                                            .Worksheets(1).Cells(lastrow, rngKoban.Offset(0, -1).Column))
        rngcopy.Copy
        'ws.Range("d2").PasteSpecial
        ws.Cells(2, ws.Cells.Find("支部コード").Column).PasteSpecial
        
        Set rngcopy = .Worksheets(1).Range(.Worksheets(1).Cells(rngKoban.Row + 1, rngKoban.Offset(0, 1).Column), _
                                            .Worksheets(1).Cells(lastrow, rngKoban.Offset(0, 4).Column))
        rngcopy.Copy
        ws.Cells(2, ws.Cells.Find("記号").Column).PasteSpecial
        
        ws.Range("J:O").NumberFormatLocal = "G/標準"
    End With
    
    With ws
      
        
        Dim c
        Dim r
        Dim cnt
        
        Dim rngKigoBango
        Set rngKigoBango = .Cells.Find("記号番号")
        Dim rngKigo
        Set rngKigo = .Cells.Find("記号", , , , , , True)
        
        
        Set rngKoban = .Cells.Find("項番")
        
        .Columns(rngKigoBango.Column).NumberFormat = "@"
        .Columns(rngKigo.Column).NumberFormat = "@"
        .Columns(rngKigo.Column + 1).NumberFormat = "@"
        
        cnt = 0
        For r = 2 To lastrow - (rngKoban.Row - 1)
            '行番号
            .Cells(r, 1).Formula = "=Row()-1"
            
            '40行で改ページ
            If cnt Mod 40 = 0 And cnt > 1 Then .Cells(r, 1).PageBreak = xlPageBreakManual
            
            '項番作成
            .Cells(r, rngKoban.Column).Formula = "=d" & r & _
                                    " & e" & r & _
                                    " & f" & r & _
                                    " & g" & r & _
                                    " & h" & r
                        
            '記号・番号
            .Cells(r, rngKigoBango.Column).Value = _
                CStr(.Cells(r, rngKigo.Column).Value & .Cells(r, rngKigo.Column + 1).Value)
            
             cnt = cnt + 1
        Next
        
        '項番を値で貼り付ける
        .Columns(rngKoban.Column).Copy
        .Columns(rngKoban.Column).PasteSpecial -4163
        
        'シートの体裁（山内希望）
        
        'フォント関連
        .Cells.Font.Name = "ＭＳ ゴシック"
        .Cells.Font.Size = 10
        
        .Columns("a:a").Font.Size = 10
        .Columns("b:b").Font.Size = 8
        .Columns("c:c").Font.Size = 12
        .Columns("e:f").Font.Size = 14
        
        
        .Columns.AutoFit
        
        '各行の高さ
        .UsedRange.RowHeight = 20
        
        
        '外国語の広さ
        '.Columns("s:s").ColumnWidth = 10
        .Columns(.Cells.Find("外国語").Column).ColumnWidth = 10
        
        
        'メモの広さ
        '.Columns("u:u").ColumnWidth = 20
        .Columns(.Cells.Find("memo1").Column).ColumnWidth = 20
        
        
        'ヘッダ行
        .Rows("1:1").RowHeight = 35
        .Rows("1:1").WrapText = True
    
        '列の広さ
        '.Columns("c:d").ColumnWidth = 7
        .Columns(.Cells.Find("セット番号").Column).ColumnWidth = 5
        .Columns(.Cells.Find("支部コード").Column).ColumnWidth = 5
        
        .Columns("g:h").ColumnWidth = 5
    
    
        '枠線
        .UsedRange.Borders.LineStyle = 1
        .UsedRange.Borders.Weight = 1
        
        '印刷設定
        .PageSetup.Zoom = 70
        .PageSetup.Orientation = 2
        .PageSetup.TopMargin = 20
        .PageSetup.BottomMargin = 30
        .PageSetup.RightMargin = 10
        .PageSetup.LeftMargin = 10
        
        .PageSetup.PrintTitleRows = "$1:$1"
        .PageSetup.CenterFooter = "&P/&N"
        

        
    End With
    
	twb.save
	twb.close
	exapp.quit
	set exapp=nothing


	if err.number<>0 then
		msgbox err.description
		twb.close false
		exapp.quit
		set exapp=nothing

	else
		msgbox "完了"

	end if

