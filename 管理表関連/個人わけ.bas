Attribute VB_Name = "個人わけ"


Sub 個人分け()
    Dim dic() As String
    Dim lastcol As Integer
    Dim lastrow As Long
    Dim rng As Range
    
    Dim SetNoCol As Integer 'セット番号の列
    Dim KigoBangoCol As Integer '記号番号の列
    
    
    Cells.Interior.ColorIndex = xlNone '色初期化
    
    '最後取得
    lastcol = Range("a1").End(xlToRight).Column
    lastrow = Range("a1").End(xlDown).Row
    
    
    Set rng = Cells.Find("セット番号")
    SetNoCol = rng.Column
    
    Range(Cells(2, SetNoCol), Cells(lastrow, SetNoCol)).ClearContents
    
        
    Set rng = Cells.Find("記号番号")
    KigoBangoCol = rng.Column
    
    Dim c As Range
    Dim r As Long
    r = 1
    
    'dicに記号番号を入れる
    For Each c In Range(Cells(2, KigoBangoCol), Cells(lastrow, KigoBangoCol))
        
        If c.Value = "" Then Exit For
        
        Dim cnt As Integer
        Dim flg
        
        On Error GoTo 1
        'dicに既に存在する記号番号は入れない
        For cnt = 0 To UBound(dic, 2)
            If dic(0, cnt) = c.Value Then
                flg = True
                 Exit For
            Else
                flg = False
            End If
            
        Next
        
1:
        If Not flg Then
            ReDim Preserve dic(1, r)
            dic(0, r) = c.Value
            dic(1, r) = Format(CStr(r), "000")
            r = r + 1
        End If
         
    Next
    
    '最初から回す
    For r = 0 To lastrow
    
        '記号番号と比較する
        Set c = Cells(r + 1, KigoBangoCol)
        
        For cnt = 1 To UBound(dic, 2)
            
            If dic(0, cnt) = c.Value Then
                'セット番号入れる
                Cells(r + 1, SetNoCol).Value = dic(1, cnt)
                
                '色つけ
                If dic(1, cnt) Mod 2 = 0 Then Rows(c.Row).Interior.ColorIndex = 35
                If dic(1, cnt) Mod 2 = 1 Then Rows(c.Row).Interior.ColorIndex = 36
            End If
        Next
    Next
    
    '最終ソート
    Cells.Sort key1:="セット番号", order1:=xlAscending, _
    key2:="委託区分", order2:=xlDescending, _
    key3:="月内通番", order3:=xlAscending, _
    Header:=xlYes
    
    
End Sub

